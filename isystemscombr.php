<?php
/**
 * I.Systems - mu-plugin
 *
 * PHP version 7
 *
 * @category  Wordpress_Mu-plugin
 * @package   I.Systems
 * @author    collab I bm <contato@blackmagenta.com.br>
 * @copyright 2021 collab I bm
 * @license   Proprietary https://blackmagenta.com.br
 * @link      https://blackmagenta.com.br
 *
 * @wordpress-plugin
 * Plugin Name: I.Systems - mu-plugin
 * Plugin URI:  https://blackmagenta.com.br
 * Description: Customizations for isystems.com.br site
 * Version:     1.1.2
 * Author:      collab I bm
 * Author URI:  https://blackmagenta.com.br/
 * Text Domain: isystemscombr
 * License:     Proprietary
 * License URI: https://blackmagenta.com.br
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

/**
 * Load Translation
 *
 * @return void
 */
add_action(
    'plugins_loaded',
    function () {
        load_muplugin_textdomain('isystemscombr', basename(dirname(__FILE__)).'/languages');
    }
);

/**
 * Register Polylang Strings for Translation
 *
 * @return void
 */
add_action(
    'plugins_loaded', 
    function () {
        if (function_exists('pll_register_string')) {
            pll_register_string('arrasteparaolado', 'Arraste para o lado', 'i.Systems', false);
            pll_register_string('vermapa', 'ver mapa', 'i.Systems', false);
            pll_register_string('copyright', 'Todos os direitos reservados', 'i.Systems', false);
            pll_register_string('privacypolicy', 'Política de Privacidade', 'i.Systems', false);
            pll_register_string('abrirmenu', 'Abrir Menu', 'i.Systems', false);
            pll_register_string('fecharmenu', 'Fechar Menu', 'i.Systems', false);            
            pll_register_string('fechar', 'Fechar', 'i.Systems', false);
            pll_register_string('vermais', 'Ver mais', 'i.Systems', false);
            pll_register_string('vejamais', 'Veja mais', 'i.Systems', false);
            pll_register_string('saibamais', 'Saiba mais', 'i.Systems', false);
            pll_register_string('vejatambem', 'Veja também', 'i.Systems', false);
            pll_register_string('filtros', 'Filtros', 'i.Systems', false);
            pll_register_string('filtrarpor', 'Filtrar por', 'i.Systems', false);
            pll_register_string('industria', 'Indústria', 'i.Systems', false);
            pll_register_string('buscar', 'Buscar', 'i.Systems', false);
            pll_register_string('saibacomo', 'Saiba como', 'i.Systems', false);
            pll_register_string('desafio', 'Desafio', 'i.Systems', false);
            pll_register_string('solucao', 'Solução', 'i.Systems', false);
            pll_register_string('resultado', 'Resultado', 'i.Systems', false);
            pll_register_string('categorias', 'Categorias', 'i.Systems', false);
            pll_register_string('leiamais', 'Leia mais', 'i.Systems', false);
            pll_register_string('carregarmaisposts', 'Carregar mais posts', 'i.Systems', false);
            pll_register_string('por', 'Por', 'i.Systems', false);
            pll_register_string('voltarparaoblog', 'Voltar para o Blog', 'i.Systems', false);
            pll_register_string('voltarparacases', 'Voltar para Cases', 'i.Systems', false);
            pll_register_string('semresultados', 'Desculpe, não foram encontrados resultados.', 'i.Systems', false);
            pll_register_string('previous', 'Anterior', 'i.Systems', false);
            pll_register_string('facebook-link', 'Facebook (abre em nova guia)', 'i.Systems', false);
            pll_register_string('linkedin-link', 'LinkedIn (abre em nova guia)', 'i.Systems', false);
            pll_register_string('youtube-link', 'Youtube (abre em nova guia)', 'i.Systems', false);
            pll_register_string('menusolucoesparent', 'menu-solucoes-parent', 'i.Systems', false);
            pll_register_string('cookie-notice-msg', 'Utilizamos cookies para oferecer melhor experiência, melhorar o desempenho, analisar como você interage em nosso site e personalizar conteúdo. Ao utilizar este site, você concorda com o uso de cookies. Conheça nossa', 'i.Systems', false);
            pll_register_string('cookie-notice-btn-text', 'Continuar', 'i.Systems', false);
        };
    }
);

/**************************************************************************************************************
 * Functions
 **************************************************************************************************************/

/**
 * Show Industry Sectors
 * Return a array of isystems_sector custom post type
 */
function Show_Isystems_sectors() 
{
    if (post_type_exists('isystems_sector')) {
        $args = array(
            'posts_per_page'   => '-1',
            'orderby'          => 'title',
            'order'            => 'ASC',
            'post_type'        => 'isystems_sector',
            'post_status'      => 'publish',
            'suppress_filters' => true,
            'fields'           => '',
        );
        $sectors_posts = get_posts($args);

        $sectors = array();
        foreach ($sectors_posts as $sector) {
            $sectors[$sector->ID] = $sector->post_title;
        }

        if (!empty($sectors)) {
            return $sectors;
        } else {
            return array(
                'none' => __('No sectors found', 'isystemscombr'),
            );
        }

    } else {
        return array(
            'none' => __('No sectors type', 'isystemscombr'),
        );
    } 
}

/**
 * Get a list of posts
 *
 * Generic function to return an array of posts formatted for CMB2. Simply pass
 * in your WP_Query arguments and get back a beautifully formatted CMB2 options
 * array.
 *
 * @param array $query_args WP_Query arguments
 * @return array CMB2 options array
 */
function Get_Post_array($query_args = array(), $exclude_current_post = false, $add_none = false) 
{
    $lang = function_exists('pll_current_language') && isset($_GET['post']) ? pll_get_post_language($_GET['post']) : '';
    $exclude_id = $exclude_current_post && isset($_GET['post']) ? array($_GET['post']) : '';

    $defaults = array(
        'posts_per_page' => -1,
        'orderby'          => 'title',
        'order'            => 'ASC',
        'post_status'      => 'publish',
        'post__not_in'     => $exclude_id,
        'lang'             => $lang
    );
    $query = new WP_Query(array_replace_recursive($defaults, $query_args));
    
    if ($add_none) {
        return array("-1" => '') + wp_list_pluck($query->get_posts(), 'post_title', 'ID');
    } else {
        return wp_list_pluck($query->get_posts(), 'post_title', 'ID');
    }
    
}

/***********************************************************************************
 * Callback Functions
 **********************************************************************************/

/**********
 * Show on Front Page
 *
 * @return bool $display
 */
function Show_On_Front_page($cmb)
{
    // Get ID of page set as front page, 0 if there isn't one
    $front_page = get_option('page_on_front');

     // Get the translated page of the curent language
    $translated_page = function_exists('pll_get_post') ? pll_get_post($front_page) : $front_page;

    // There is a front page set  - are we editing it?
    return $cmb->object_id() == $translated_page;
}

/**********
 * Show on Home / Blog
 *
 * @return bool $display
 */
function Show_On_Home($cmb)
{
    // Get ID of page set as home, 0 if there isn't one
    $home = get_option('page_for_posts');

     // Get the translated page of the curent language
    $translated_page = function_exists('pll_get_post') ? pll_get_post($home) : $home;

    // There is a home set  - are we editing it?
    return $cmb->object_id() == $translated_page;
}

/**********
 * Show on Privacy Policy 
 *
 * @return bool $display
 */
function Show_On_Privacy($cmb)
{
    // Get ID of page set as privacy, 0 if there isn't one
    $privacy = get_option('wp_page_for_privacy_policy');

     // Get the translated page of the curent language
    $translated_page = function_exists('pll_get_post') ? pll_get_post($privacy) : $privacy;

    // There is a privacy set  - are we editing it?
    return $cmb->object_id() == $translated_page;
}


/**
 * Show on Plan
 *
 * @return bool $display
 *
 * @author Tom Morton
 * @link   https://github.com/CMB2/CMB2/wiki/Adding-your-own-show_on-filters
 */
function Cmb2_Show_On_plan()
{
    $post_id = 0;

    // If we're showing it based on ID, get the current ID
    if (isset($_GET['post'])) {
        $post_id = $_GET['post'];
    } elseif (isset($_POST['post_ID'])) {
        $post_id = $_POST['post_ID'];
    }

    if (!$post_id) {
        return false;
    }

    $slug = get_post($post_id)->post_name;

    // See if there's a match
    $slugs = array("combos", "internet", "tv"); 

    $parents = get_post_ancestors($post_id);

    return in_array($slug, $slugs) AND (!$parents);
}

 /**
  * Metabox for Page Slug
  *
  * @param bool  $display  Display
  * @param array $meta_box Metabox 
  *
  * @return bool $display
  *
  * @author Tom Morton
  * @link   https://github.com/CMB2/CMB2/wiki/Adding-your-own-show_on-filters
  */
function Cmb2_Metabox_Show_On_slug($display, $meta_box)
{
    if (!isset($meta_box['show_on']['key'], $meta_box['show_on']['value'])) {
        return $display;
    }

    if ('slug' !== $meta_box['show_on']['key']) {
        return $display;
    }

    $post_id = 0;

    // If we're showing it based on ID, get the current ID
    if (isset($_GET['post'])) {
        $post_id = $_GET['post'];
    } elseif (isset($_POST['post_ID'])) {
        $post_id = $_POST['post_ID'];
    }

    if (!$post_id) {
        return $display;
    }

    $slug = get_post($post_id)->post_name;

    $parent_slug = true;
    if (isset($meta_box['show_on']['parant_slug'])) {
        $parents = get_post_ancestors($post_id); 
        $id = ($parents) ? $parents[count($parents)-1]: false;
        /* Get the parent and set the $class with the page slug (post_name) */
        if ($id) {
            $parent = get_post($id);
            $parent_slug = $parent->post_name == $meta_box['show_on']['parent_slug'];
        }
    }

    // See if there's a match
    return in_array($slug, (array) $meta_box['show_on']['value']) AND $parent_slug;
}
add_filter('cmb2_show_on', 'Cmb2_Metabox_Show_On_slug', 10, 2);

/**
 * Removes metabox from appearing on post new screens before the post
 * ID has been set.
 *
 * @param bool  $display
 * @param array $meta_box The array of metabox options
 * 
 * @return bool $display True on success, false on failure
 * 
 * @author Thomas Griffin
 */
function Cmb2_Exclude_From_New($display, $meta_box)
{
    if (!isset($meta_box['show_on']['alt_key'], $meta_box['show_on']['alt_value'])) {
        return $display;
    }

    if ('exclude_new' !== $meta_box['show_on']['alt_key']) {
        return $display;
    }

    global $pagenow;

    // Force to be an array
    $to_exclude = !is_array($meta_box['show_on']['alt_value'])
        ? array($meta_box['show_on']['alt_value'])
        : $meta_box['show_on']['alt_value'];

    $is_new_post = 'post-new.php' == $pagenow && in_array('post-new.php', $to_exclude);

    return ! $is_new_post;
}
add_filter('cmb2_show_on', 'Cmb2_Exclude_From_New', 10, 2);

/**
 * Exclude metabox on non top level posts
 *
 * @param bool $display
 * @param array $meta_box
 * 
 * @return bool display metabox
 * 
 * @author Travis Northcutt
 * @link   https://gist.github.com/gists/2039760
 */
function Cmb2_Metabox_Add_For_Top_Level_Posts_only($display, $meta_box)
{
    if (!isset($meta_box['show_on']['top_key']) || 'parent-id' !== $meta_box['show_on']['top_key']) {
        return $display;
    }

    $post_id = 0;
    
    // If we're showing it based on ID, get the current ID
    if (isset($_GET['post'])) {
        $post_id = $_GET['post'];
    } elseif (isset($_POST['post_ID'])) {
        $post_id = $_POST['post_ID'];
    }
    
    if (!$post_id) {
        return $display;
    }
    
    // If the post doesn't have ancestors, show the box
    if (!get_post_ancestors($post_id)) {
        return $display;
    }
    
    // Otherwise, it's not a top level post, so don't show it
    return false;
}
add_filter('cmb2_show_on', 'Cmb2_Metabox_Add_For_Top_Level_Posts_only', 10, 2);

/**
 * Gets a number of terms and displays them as options
 *
 * @param CMB2_Field $field CMB2 Field
 *
 * @return array An array of options that matches the CMB2 options array
 */
function Cmb2_getTermOptions($field)
{
    $args = $field->args('get_terms_args');
    $args = is_array($args) ? $args : array();

    $args = wp_parse_args($args, array('taxonomy' => 'category'));

    $taxonomy = $args['taxonomy'];

    $terms = (array) cmb2_utils()->wp_at_least('4.5.0')
        ? get_terms($args)
        : get_terms($taxonomy, $args);

    // Initate an empty array
    $term_options = array();
    if (!empty($terms)) {
        foreach ($terms as $term) {
            $term_options[ $term->term_id ] = $term->name;
        }
    }
    return $term_options;
}

/***********************************************************************************
 * Register taxonomies
 * ********************************************************************************/

/************
 * Solutions
 ***********/ 
function ct_isystems_case_solution() 
{
    $labels = array(
        'name' => __('Solutions', 'isystemscombr'),
        'singular_name' => __('Solution', 'isystemscombr'),
        'search_items' =>  __('Search Solutions', 'isystemscombr'),
        'popular_items' => __('Popular Solutions', 'isystemscombr'),
        'all_items' => __('All Solutions', 'isystemscombr'),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __('Edit Solution', 'isystemscombr'), 
        'update_item' => __('Update Solution', 'isystemscombr'),
        'add_new_item' => __('Add New Solution', 'isystemscombr'),
        'new_item_name' => __('New Solution Name', 'isystemscombr'),
        'separate_items_with_commas' => __('Separate solutions with commas', 'isystemscombr'),
        'add_or_remove_items' => __('Add or remove solutions', 'isystemscombr'),
        'choose_from_most_used' => __('Choose from the most used solutions', 'isystemscombr'),
        'menu_name' => __('Solutions', 'isystemscombr'),
    ); 
    
    // Now register the non-hierarchical taxonomy like tag
    
    register_taxonomy('case_solution', 'isystems_case', 
        array(
            'hierarchical' => false,
            'labels' => $labels,
            'show_ui' => true,
            'show_in_rest' => true,
            'show_admin_column' => true,
            'show_in_nav_menus'     => false,
            'update_count_callback' => '_update_post_term_count',
            'query_var' => true,
        )
    );
}
add_action('init', 'ct_isystems_case_solution', 0);


/***********************************************************************************
 * Register post types
 * ********************************************************************************/

/**********
 * Cases
 *********/
function cpt_isystems_case() 
{
    $labels = array(
        'name'                  => __('Cases', 'isystemscombr'),
        'singular_name'         => __('Case', 'isystemscombr'),
        'menu_name'             => __('Cases', 'isystemscombr'),
        'name_admin_bar'        => __('Case', 'isystemscombr'),
        'archives'              => __('Case Archives', 'isystemscombr'),
        'attributes'            => __('Case Attributes', 'isystemscombr'),
        'parent_item_colon'     => __('Parent Case:', 'isystemscombr'),
        'all_items'             => __('All Cases', 'isystemscombr'),
        'add_new_item'          => __('Add New Case', 'isystemscombr'),
        'add_new'               => __('Add New', 'isystemscombr'),
        'new_item'              => __('New Case', 'isystemscombr'),
        'edit_item'             => __('Edit Case', 'isystemscombr'),
        'update_item'           => __('Update Case', 'isystemscombr'),
        'view_item'             => __('View Case', 'isystemscombr'),
        'view_items'            => __('View Cases', 'isystemscombr'),
        'search_items'          => __('Search Case', 'isystemscombr'),
        'not_found'             => __('Not found', 'isystemscombr'),
        'not_found_in_trash'    => __('Not found in Trash', 'isystemscombr'),
        'featured_image'        => __('Featured Image', 'isystemscombr'),
        'set_featured_image'    => __('Set featured image', 'isystemscombr'),
        'remove_featured_image' => __('Remove featured image', 'isystemscombr'),
        'use_featured_image'    => __('Use as featured image', 'isystemscombr'),
        'insert_into_item'      => __('Insert into case', 'isystemscombr'),
        'uploaded_to_this_item' => __('Uploaded to this case', 'isystemscombr'),
        'items_list'            => __('Cases list', 'isystemscombr'),
        'items_list_navigation' => __('Cases list navigation', 'isystemscombr'),
        'filter_items_list'     => __('Filter cases list', 'isystemscombr'),
    );
    $rewrite = array(
        'slug'                  => 'cases',
        'with_front'            => true,
        'pages'                 => true,
        'feeds'                 => true,
    );
    $args = array(
        'label'                 => __('Case', 'isystemscombr'),
        'description'           => __('I.Systems Cases', 'isystemscombr'),
        'labels'                => $labels,
        'supports'              => array('title', 'thumbnail', 'revisions', 'page-attributes'),
        'taxonomies'            => array('solutions'),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 20,
        'menu_icon'             => 'data:image/svg+xml;base64,'. base64_encode('<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 45 45"><path fill="#000" d="M30.03 13.27c-2.936-2.356-6.726-3.174-10.42-2.308-4.311 1.01-7.77 4.47-8.811 8.846-1.185 4.855.473 9.76 4.358 12.788.284.24.473.673.473 1.154v6.587c0 1.346 1.09 2.451 2.416 2.451h1.516C19.988 44.087 21.125 45 22.546 45s2.558-.962 2.985-2.212h1.515c1.327 0 2.416-1.105 2.416-2.451v-6.635c0-.433.142-.817.427-1.058 3.031-2.355 4.784-5.913 4.784-9.711-.048-3.798-1.753-7.308-4.642-9.664Zm-3.552 26.538H18.52v-2.789h7.958v2.789Zm1.563-9.52c-.995.77-1.61 2.02-1.61 3.414v.385H18.52v-.337c0-1.394-.616-2.692-1.61-3.51-2.938-2.307-4.216-6.01-3.316-9.76.805-3.268 3.41-5.864 6.679-6.634 2.842-.673 5.731-.048 7.958 1.731a9.276 9.276 0 0 1 3.505 7.308c-.047 2.932-1.374 5.625-3.695 7.403ZM22.502 7.644c.805 0 1.468-.673 1.468-1.49V1.49c0-.817-.663-1.49-1.468-1.49-.806 0-1.469.673-1.469 1.49v4.664c0 .817.663 1.49 1.469 1.49Zm-10.566 4.423a1.525 1.525 0 0 0 0-2.115L8.668 6.635a1.471 1.471 0 0 0-2.084 0 1.525 1.525 0 0 0 0 2.115l3.268 3.317c.284.289.663.433 1.042.433.38 0 .758-.144 1.042-.433Zm-5.873 9.279H1.468c-.805 0-1.468.673-1.468 1.49 0 .818.663 1.49 1.468 1.49h4.595c.806 0 1.469-.672 1.469-1.49 0-.817-.663-1.49-1.469-1.49Zm37.469 0h-4.595c-.805 0-1.468.673-1.468 1.49 0 .818.663 1.49 1.468 1.49h4.595c.805 0 1.469-.672 1.469-1.49 0-.817-.664-1.49-1.469-1.49ZM38.417 6.683a1.471 1.471 0 0 0-2.084 0L33.064 10a1.525 1.525 0 0 0 0 2.115c.284.289.663.433 1.042.433.38 0 .758-.144 1.042-.433l3.269-3.317c.568-.625.568-1.539 0-2.115Z"/><path fill="#000" d="m24.963 20.24-3.458 3.51-1.468-1.49c-.616-.626-1.563-.626-2.132 0-.616.624-.616 1.586 0 2.163l2.558 2.596c.284.289.663.433 1.09.433.379 0 .758-.145 1.09-.433l4.547-4.615c.615-.625.615-1.587 0-2.164-.664-.577-1.611-.577-2.227 0Z"/></svg>'),
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'rewrite'               => $rewrite,
        'capability_type'       => 'post',
    );
    register_post_type('isystems_case', $args);

}
add_action('init', 'cpt_isystems_case', 0);

/*****
 * Solutions
 */
function cpt_isystems_solution() 
{
    $labels = array(
        'name'                  => __('Solutions', 'isystemscombr'),
        'singular_name'         => __('Solution', 'isystemscombr'),
        'menu_name'             => __('Solutions', 'isystemscombr'),
        'name_admin_bar'        => __('Solution', 'isystemscombr'),
        'archives'              => __('Solution Archives', 'isystemscombr'),
        'attributes'            => __('Solution Attributes', 'isystemscombr'),
        'parent_item_colon'     => __('Parent Solution:', 'isystemscombr'),
        'all_items'             => __('All Solutions', 'isystemscombr'),
        'add_new_item'          => __('Add New Solution', 'isystemscombr'),
        'add_new'               => __('Add New', 'isystemscombr'),
        'new_item'              => __('New Solution', 'isystemscombr'),
        'edit_item'             => __('Edit Solution', 'isystemscombr'),
        'update_item'           => __('Update Solution', 'isystemscombr'),
        'view_item'             => __('View Solution', 'isystemscombr'),
        'view_items'            => __('View Solutions', 'isystemscombr'),
        'search_items'          => __('Search Solution', 'isystemscombr'),
        'not_found'             => __('Not found', 'isystemscombr'),
        'not_found_in_trash'    => __('Not found in Trash', 'isystemscombr'),
        'featured_image'        => __('Featured Image', 'isystemscombr'),
        'set_featured_image'    => __('Set featured image', 'isystemscombr'),
        'remove_featured_image' => __('Remove featured image', 'isystemscombr'),
        'use_featured_image'    => __('Use as featured image', 'isystemscombr'),
        'insert_into_item'      => __('Insert into solution', 'isystemscombr'),
        'uploaded_to_this_item' => __('Uploaded to this solution', 'isystemscombr'),
        'items_list'            => __('Solutions list', 'isystemscombr'),
        'items_list_navigation' => __('Solutions list navigation', 'isystemscombr'),
        'filter_items_list'     => __('Filter solutions list', 'isystemscombr'),
    );
    $rewrite = array(
        'slug'                  => 'solucoes',
        'with_front'            => true,
        'pages'                 => true,
        'feeds'                 => true,
    );
    $args = array(
        'label'                 => __('Solution', 'isystemscombr'),
        'description'           => __('I.Systems Solutions', 'isystemscombr'),
        'labels'                => $labels,
        'supports'              => array('title', 'thumbnail', 'revisions', 'page-attributes'),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 20,
        'menu_icon'             => 'data:image/svg+xml;base64,'. base64_encode('<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 64 64"><path stroke="#000" stroke-linejoin="round" stroke-width="3.5" d="M57.013 6H45.398a1.915 1.915 0 0 0-1.915 1.787l-1.531 23.358H32.25v-7.403a1.916 1.916 0 0 0-3-1.532l-10.21 7.212v-5.68a1.916 1.916 0 0 0-3-1.532L2.766 31.529A1.915 1.915 0 0 0 2 33.06v22.72a1.915 1.915 0 0 0 1.915 1.915h56.162a1.914 1.914 0 0 0 1.404-.638 1.724 1.724 0 0 0 .51-1.404L58.928 7.787A2.042 2.042 0 0 0 57.013 6Z"/><path stroke="#000" stroke-linejoin="round" stroke-width="3.5" d="M11 49.5h12V39H11v10.5Z"/></svg>'),
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => false,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'rewrite'               => $rewrite,
        'capability_type'       => 'page',
    );
    register_post_type('isystems_solution', $args);
}
add_action('init', 'cpt_isystems_solution', 0);

/*****
 * Sectors
 ***/
function cpt_isystems_sector()
{
    $labels = array(
        'name'                  => __('Sectors', 'isystemscombr'),
        'singular_name'         => __('Sector', 'isystemscombr'),
        'menu_name'             => __('Sectors', 'isystemscombr'),
        'name_admin_bar'        => __('Sector', 'isystemscombr'),
        'archives'              => __('Sector Archives', 'isystemscombr'),
        'attributes'            => __('Sector Attributes', 'isystemscombr'),
        'parent_item_colon'     => __('Parent Sector:', 'isystemscombr'),
        'all_items'             => __('All Sectors', 'isystemscombr'),
        'add_new_item'          => __('Add New Sector', 'isystemscombr'),
        'add_new'               => __('Add New', 'isystemscombr'),
        'new_item'              => __('New Sector', 'isystemscombr'),
        'edit_item'             => __('Edit Sector', 'isystemscombr'),
        'update_item'           => __('Update Sector', 'isystemscombr'),
        'view_item'             => __('View Sector', 'isystemscombr'),
        'view_items'            => __('View Sectors', 'isystemscombr'),
        'search_items'          => __('Search Sector', 'isystemscombr'),
        'not_found'             => __('Not found', 'isystemscombr'),
        'not_found_in_trash'    => __('Not found in Trash', 'isystemscombr'),
        'featured_image'        => __('Featured Image', 'isystemscombr'),
        'set_featured_image'    => __('Set featured image', 'isystemscombr'),
        'remove_featured_image' => __('Remove featured image', 'isystemscombr'),
        'use_featured_image'    => __('Use as featured image', 'isystemscombr'),
        'insert_into_item'      => __('Insert into sector', 'isystemscombr'),
        'uploaded_to_this_item' => __('Uploaded to this sector', 'isystemscombr'),
        'items_list'            => __('Sectors list', 'isystemscombr'),
        'items_list_navigation' => __('Sectors list navigation', 'isystemscombr'),
        'filter_items_list'     => __('Filter sectors list', 'isystemscombr'),
    );
    $rewrite = array(
        'slug'                  => 'setores',
        'with_front'            => false,
        'pages'                 => true,
        'feeds'                 => true,
    );
    $args = array(
        'label'                 => __('Sector', 'isystemscombr'),
        'description'           => __('Industry Sectors', 'isystemscombr'),
        'labels'                => $labels,
        'supports'              => array('title', 'page-attributes'),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 20,
        'menu_icon'             => 'data:image/svg+xml;base64,'. base64_encode('<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 44 44"><path fill="#000" d="M29.977 9.001a.932.932 0 0 0-.756.553l-5.41 12.275-20.144-4.858 3.068-3.432h18.603a.931.931 0 0 0 .026-1.862H6.328a.932.932 0 0 0-.698.306l-4.393 4.916a.932.932 0 0 0 .48 1.527l5.527 1.323-2.268 1.382a.93.93 0 0 0-.452.8v2.051a.93.93 0 1 0 1.862.026v-1.553l3.433-2.08 14.326 3.447a.931.931 0 0 0 1.077-.524l5.454-12.435h11.098A.931.931 0 1 0 41.8 9H29.977Z"/><path fill="#000" d="m39.373 19.706-3.87-4.261a.93.93 0 0 0-.683-.306h-3.927a.845.845 0 0 0-.087 0 .93.93 0 0 0-.843.931v4.261a.93.93 0 0 0 .93.931h7.797a.93.93 0 0 0 .683-1.556Zm-7.55-.305V17h2.575l2.182 2.4h-4.756ZM9.514 24.55c-2.626 0-4.756 2.151-4.756 4.785 0 2.633 2.13 4.8 4.756 4.8 2.626 0 4.77-2.167 4.77-4.8 0-2.634-2.144-4.786-4.77-4.786Zm0 7.723c-1.613 0-2.894-1.305-2.894-2.938 0-1.634 1.281-2.924 2.894-2.924a2.9 2.9 0 0 1 2.909 2.924 2.913 2.913 0 0 1-2.91 2.938Z"/><path fill="#000" d="m43.576 18.848-6.268-6.894a.932.932 0 0 0-.742-.32.968.968 0 0 0-.102.014.93.93 0 0 0-.539 1.556l6.037 6.633v8.538h-2.575c-.447-2.177-2.371-3.826-4.669-3.826-2.297 0-4.222 1.65-4.668 3.826H16.276a.931.931 0 1 0 0 1.862h13.759c.423 2.207 2.365 3.897 4.683 3.897 2.319 0 4.26-1.69 4.683-3.898h3.491a.93.93 0 0 0 .931-.93v-9.818a.93.93 0 0 0-.247-.64Zm-8.858 13.425c-1.613 0-2.908-1.305-2.908-2.938a2.9 2.9 0 0 1 2.908-2.924 2.9 2.9 0 0 1 2.91 2.924 2.913 2.913 0 0 1-2.91 2.938ZM9.512 28.287c-.575 0-1.032.468-1.032 1.048 0 .579.457 1.046 1.032 1.046s1.048-.468 1.048-1.046c0-.58-.472-1.048-1.048-1.048Z"/><path fill="#000" d="M34.718 28.287c-.575 0-1.033.468-1.033 1.048 0 .579.458 1.046 1.033 1.046s1.047-.468 1.047-1.046c0-.58-.472-1.048-1.047-1.048Z"/></svg>'),
        'show_in_admin_bar'     => false,
        'show_in_nav_menus'     => false,
        'can_export'            => true,
        'has_archive'           => false,
        'exclude_from_search'   => true,
        'publicly_queryable'    => false,
        'rewrite'               => $rewrite,
        'capability_type'       => 'page',
    );
    register_post_type('isystems_sector', $args);
}
add_action('init', 'cpt_isystems_sector', 0);


/***********************************************************************************
 * CPT Fields
 * ********************************************************************************/

 /******* 
  * Single Case
  ********/
function cmb_isystems_case() 
{
    $prefix = '_isystems_case_';
    
    /**
    * Featured Banner / Hero
    */
    $cmb = new_cmb2_box(
        array(
            'id'            => 'isystems_case_hero_id',
            'title'         => __('Featured Banner', 'isystemscombr'),
            'object_types'  => array('isystems_case'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, 
        )
    );

    //Background Image
    $cmb->add_field(
        array(
            'name'    => __('Background Image', 'isystemscombr'),
            'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 2880x916px | Aspect ratio: 22:7', 'isystemscombr'),
            'id'      => $prefix .'hero_bkg_img',
            'type'    => 'file',
            'options' => array(
                'url' => false, 
            ),
            'text'    => array(
                    'add_upload_file_text' => __('Add Image', 'isystemscombr'), 
            ),
            'query_args' => array(
                'type' => array(
                    'image/jpeg',
                    'image/png',
                    //'image/svg+xml',
                ),
            ),
            'preview_size' => array(288, 91) 
        ) 
    );

    //Logo
    $cmb->add_field(
        array(
            'name'    => __('Logo', 'isystemscombr'),
            'desc'    => __('Format: SVG (recommended), PNG | Recommended size: 640x320px | Aspect ratio: 2:1', 'isystemscombr'),
            'id'      => $prefix .'logo',
            'type'    => 'file',
            'options' => array(
                'url' => false, 
            ),
            'text'    => array(
                'add_upload_file_text' => __('Add Image', 'isystemscombr'), 
            ),
            'query_args' => array(
                'type' => array(
                    //'image/jpeg',
                    'image/png',
                    'image/svg+xml',
                ),
            ),
            'preview_size' => array(240, 120) 
        ) 
    );

    /**
    * Headline
    */
    $cmb_headline = new_cmb2_box(
        array(
            'id'            => 'isystems_case_headline_id',
            'title'         => __('Headline', 'isystemscombr'),
            'object_types'  => array('isystems_case'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, 
        )
    );

    //Title
    $cmb_headline->add_field( 
        array(
            'name'       => __('Title', 'isystemscombr'),
            'desc'       => '',
            'id'         => $prefix . 'headline_title',
            'type'       => 'text',
        )
    );

    //Description
    $cmb_headline->add_field( 
        array(
            'name'       => __('Description', 'isystemscombr'),
            'desc'       => '',
            'id'         => $prefix . 'headline_desc',
            'type'       => 'wysiwyg',
            'options' => array(
                
                'media_buttons' => false,
                'textarea_rows' => get_option('default_post_edit_rows', 5),
                'tabindex' => '',
                'editor_css' => '',
                'editor_class' => '',
                'teeny' => true,
                'tinymce' => true,
                'quicktags' => true
            ),
        )
    );

    //Topics Group
    $topics_id = $cmb_headline->add_field(
        array(
            'id'          => $prefix . 'headline_topics',
            'type'        => 'group',
            'description' => __('Illustrated Topics', 'isystemscombr'),
            'options'     => array(
                'group_title'   => __('Topic {#}', 'isystemscombr'),
                'add_button'    => __('Add Another Topic', 'isystemscombr'),
                'remove_button' => __('Remove Topic', 'isystemscombr'),
                'sortable'      => true,
            ),
        )
    );

    //Topic Icon
    $cmb_headline->add_group_field(
        $topics_id,
        array (
            'name'    => __('Icon', 'isystemscombr'),
            'desc'    => __('Format: SVG (recommended), PNG | Recommended size: 320x320 | Aspect ratio: 1:1', 'isystemscombr'),
            'id'      => 'icon',
            'type'    => 'file',
            'options' => array(
                'url' => false, 
            ),
            'text'    => array(
                'add_upload_file_text' => __('Add Image', 'isystemscombr'), 
            ),
            'query_args' => array(
                'type' => array(
                    //'image/jpeg',
                    'image/png',
                    'image/svg+xml',
                ),
            ),
            'preview_size' => array(64, 64) 
        ) 
    );

    // Topic Title
    $cmb_headline->add_group_field(
        $topics_id,
        array (
            'name'       => __('Title', 'isystemscombr'),
            'desc'       => '',
            'id'         => 'title',
            'type'       => 'text',
        )
    );
    
    // Topic Description
    $cmb_headline->add_group_field(
        $topics_id,
        array (
            'name'       => __('Description', 'isystemscombr'),
            'desc'       => '',
            'id'         => 'desc',
            'type'       => 'text',
        )
    );

    /**
    * Company
    */
    $cmb_company = new_cmb2_box(
        array(
            'id'            => 'isystems_case_company_id',
            'title'         => __('Company', 'isystemscombr'),
            'object_types'  => array('isystems_case'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, 
        )
    );

    //Featured Image
    $cmb_company->add_field(
        array(
            'name'    => __('Featured Image', 'isystemscombr'),
            'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 1024x1323px | Aspect ratio: 24:31', 'isystemscombr'),
            'id'      => $prefix .'company_img',
            'type'    => 'file',
            'options' => array(
                'url' => false, 
            ),
            'text'    => array(
                'add_upload_file_text' => __('Add Image', 'isystemscombr'), 
            ),
            'query_args' => array(
                'type' => array(
                    'image/jpeg',
                    'image/png',
                    //'image/svg+xml',
                ),
            ),
            'preview_size' => array(102, 132) 
        )
    ); 

    //Name
    $cmb_company->add_field( 
        array(
            'name'       => __('Name', 'isystemscombr'),
            'desc'       => '',
            'id'         => $prefix . 'company_name',
            'type'       => 'text',
        )
    );

    //Desc
    $cmb_company->add_field( 
        array(
            'name'       => __('Description', 'isystemscombr'),
            'desc'       => '',
            'id'         => $prefix . 'company_desc',
            'type'       => 'wysiwyg',
            'options' => array(
                
                'media_buttons' => false,
                'textarea_rows' => get_option('default_post_edit_rows', 5),
                'tabindex' => '',
                'editor_css' => '',
                'editor_class' => '',
                'teeny' => true,
                'tinymce' => true,
                'quicktags' => true
            ),
        )
    );

    /**
    * Challenge
    */
    $cmb_challenge = new_cmb2_box(
        array(
            'id'            => 'isystems_case_challenge_id',
            'title'         => __('Challenge', 'isystemscombr'),
            'object_types'  => array('isystems_case'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, 
        )
    );

    //Title
    $cmb_challenge->add_field( 
        array(
            'name'       => __('Title', 'isystemscombr'),
            'desc'       => '',
            'id'         => $prefix . 'challenge_title',
            'type'       => 'text',
        )
    );

    //Text
    $cmb_challenge->add_field( 
        array(
            'name'       => __('Description', 'isystemscombr'),
            'desc'       => '',
            'id'         => $prefix . 'challenge_desc',
            'type'       => 'wysiwyg',
            'options' => array(
                
                'media_buttons' => false,
                'textarea_rows' => get_option('default_post_edit_rows', 5),
                'tabindex' => '',
                'editor_css' => '',
                'editor_class' => '',
                'teeny' => true,
                'tinymce' => true,
                'quicktags' => true
            ),
        )
    );

    //Gallery
    $cmb_challenge->add_field( 
        array(
            'name'       => __('Gallery', 'isystemscombr'),
            'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 1920x1080px | Aspect ratio: 16:9', 'isystemscombr'),
            'id'         => $prefix . 'challenge_gallery',
            'type'       => 'file_list',
            'options' => array(
                'url' => false, 
            ),
            'text'    => array(
                'add_upload_file_text' => __('Add Images', 'isystemscombr'), 
            ),
            'query_args' => array(
                'type' => array(
                    'image/jpeg',
                    'image/png',
                    //'image/svg+xml',
                ),
            ),
            'preview_size' => array(192, 108) 
        )
    );

    /**
    * Solution
    */
    $cmb_solution = new_cmb2_box(
        array(
            'id'            => 'isystems_case_solution_id',
            'title'         => __('Solution', 'isystemscombr'),
            'object_types'  => array('isystems_case'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, 
        )
    );

    //Title
    $cmb_solution->add_field( 
        array(
            'name'       => __('Title', 'isystemscombr'),
            'desc'       => '',
            'id'         => $prefix . 'solution_title',
            'type'       => 'text',
        )
    );

    //Text
    $cmb_solution->add_field( 
        array(
            'name'       => __('Description', 'isystemscombr'),
            'desc'       => '',
            'id'         => $prefix . 'solution_desc',
            'type'       => 'wysiwyg',
            'options' => array(
                
                'media_buttons' => false,
                'textarea_rows' => get_option('default_post_edit_rows', 5),
                'tabindex' => '',
                'editor_css' => '',
                'editor_class' => '',
                'teeny' => true,
                'tinymce' => true,
                'quicktags' => true
            ),
        )
    );

    /**
    * Result
    */
    $cmb_result = new_cmb2_box(
        array(
            'id'            => 'isystems_case_result_id',
            'title'         => __('Result', 'isystemscombr'),
            'object_types'  => array('isystems_case'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, 
        )
    );

    //Title
    $cmb_result->add_field( 
        array(
            'name'       => __('Title', 'isystemscombr'),
            'desc'       => '',
            'id'         => $prefix . 'result_title',
            'type'       => 'text',
        )
    );

    //Text
    $cmb_result->add_field( 
        array(
            'name'       => __('Description', 'isystemscombr'),
            'desc'       => '',
            'id'         => $prefix . 'result_desc',
            'type'       => 'wysiwyg',
            'options' => array(
                
                'media_buttons' => false,
                'textarea_rows' => get_option('default_post_edit_rows', 5),
                'tabindex' => '',
                'editor_css' => '',
                'editor_class' => '',
                'teeny' => true,
                'tinymce' => true,
                'quicktags' => true
            ),
        )
    );

    /**
    * Testimonials
    */
    $cmb_testimonials = new_cmb2_box(
        array(
            'id'            => 'isystems_case_testimonials_id',
            'title'         => __('Testimonials', 'isystemscombr'),
            'object_types'  => array('isystems_case'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, 
        )
    );

    //Testimonials Group
    $testimonial_id = $cmb_testimonials->add_field(
        array(
            'id'          => $prefix . 'testimonials_testimonial',
            'type'        => 'group',
            'description' => '',
            'options'     => array(
                'group_title'   => __('Testimonial {#}', 'isystemscombr'),
                'add_button'    => __('Add Another Testimonial', 'isystemscombr'),
                'remove_button' => __('Remove Testimonial', 'isystemscombr'),
                'sortable'      => true,
            ),
        )
    );

    // Testimonial Client
    $cmb_testimonials->add_group_field(
        $testimonial_id,
        array (
            'name'       => __('Client', 'isystemscombr'),
            'desc'       => '',
            'id'         => 'name',
            'type'       => 'text',
        )
    );

    // Testimonial Role
    $cmb_testimonials->add_group_field(
        $testimonial_id,
        array (
            'name'       => __('Role', 'isystemscombr'),
            'desc'       => '',
            'id'         => 'role',
            'type'       => 'text',
        )
    );

    // Testimonial Description
    $cmb_testimonials->add_group_field(
        $testimonial_id,
        array (
            'name'       => __('Testimonial', 'isystemscombr'),
            'desc'       => '',
            'id'         => 'content',
            'type'       => 'wysiwyg',
            'options' => array(
                
                'media_buttons' => false,
                'textarea_rows' => get_option('default_post_edit_rows', 5),
                'tabindex' => '',
                'editor_css' => '',
                'editor_class' => '',
                'teeny' => true,
                'tinymce' => true,
                'quicktags' => true
            ),
        )
    );

    /**
    * Form
    */
    $cmb_form = new_cmb2_box(
        array(
            'id'            => 'isystems_case_form_id',
            'title'         => __('Form', 'isystemscombr'),
            'object_types'  => array('isystems_case'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, 
        )
    );

    //Title
    $cmb_form->add_field( 
        array(
            'name'       => __('Title', 'isystemscombr'),
            'desc'       => '',
            'id'         => $prefix . 'form_title',
            'type'       => 'text',
        )
    );

    //Desc
    $cmb_form->add_field( 
        array(
            'name'       => __('Description', 'isystemscombr'),
            'desc'       => '',
            'id'         => $prefix . 'form_desc',
            'type'       => 'wysiwyg',
            'options' => array(
                'media_buttons' => false,
                'textarea_rows' => get_option('default_post_edit_rows', 5),
                'tabindex' => '',
                'editor_css' => '',
                'editor_class' => '',
                'teeny' => true,
                'tinymce' => true,
                'quicktags' => true
            ),
        )
    );

    //Shortcode
    $cmb_form->add_field( 
        array(
            'name'       => __('Form Shortcode', 'isystemscombr'),
            'desc'       => 'Contact Form 7 Shortcode',
            'id'         => $prefix . 'form_shortcode',
            'type'       => 'text',
        )
    );

    /***********
     * Solutions
     */
    $cmb_solutions = new_cmb2_box(
        array(
            'id'            => 'isystems_case_filter_solution_id',
            'title'         => __('Case Solution', 'isystemscombr'),
            'object_types'  => array('isystems_case'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, 
        )
    );

    $cmb_solutions->add_field( 
        array(
            'name'       => __('Solutions', 'isystemscombr'),
            'desc'       => __('Select case solution', 'isystemscombr'),
            'id'         => $prefix . 'filter_solution',
            'taxonomy'   => 'case_solution', //Enter Taxonomy Slug
            'type'       => 'taxonomy_radio',
            'text'       => array(
                'no_terms_text' => __('Sorry, no solutions could be found.', 'isystemscombr')
            ),
            'show_option_none' => false,
            'remove_default' => 'true', 
            'query_args' => array(
                'post_type' => 'isystems_case',
                //'hide_empty' => true,
            ),
        ) 
    );

    /***********
     * Industry Sector
     */
    $cmb_industry = new_cmb2_box(
        array(
            'id'            => 'isystems_case_filter_industry_id',
            'title'         => __('Case Industry Sector', 'isystemscombr'),
            'object_types'  => array('isystems_case'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, 
        )
    );

    $cmb_industry->add_field( 
        array(
            'name'       => __('Industry Sectors', 'isystemscombr'),
            'desc'       => __('Select case industry sector', 'isystemscombr'),
            'id'         => $prefix . 'filter_industry',
            'type'       => 'radio',
            'options'        => Get_Post_array(array('post_type' => 'isystems_sector')),
        ) 
    );

    /**
    * Thumbnail
    */
    $cmb_form = new_cmb2_box(
        array(
            'id'            => 'isystems_case_thumbnail_id',
            'title'         => __('Thumbnail', 'isystemscombr'),
            'object_types'  => array('isystems_case'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, 
        )
    );

    //Description
    $cmb_form->add_field( 
        array(
            'name'       => __('Description', 'isystemscombr'),
            'desc'       => __('Description for case thumbnail', 'isystemscombr'),
            'id'         => $prefix . 'thumbnail_desc',
            'type'       => 'textarea_small',
        )
    );
}
add_action('cmb2_admin_init', 'cmb_isystems_case');

/**** 
 * Solutions
 */
function cmb_isystems_solution() 
{
    $prefix = '_isystems_solution_';
    
    /**
    * Featured Banner (Hero)
    */
    $cmb = new_cmb2_box(
        array(
            'id'            => 'isystems_solution_hero_id',
            'title'         => __('Featured Banner', 'isystemscombr'),
            'object_types'  => array('isystems_solution'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, 
        )
    );

    //Background Image
    $cmb->add_field(
        array(
            'name'    => __('Background Image', 'isystemscombr'),
            'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 2880x1000px | Aspect ratio: 72:25', 'isystemscombr'),
            'id'      => $prefix .'hero_bkg_img',
            'type'    => 'file',
            'options' => array(
                'url' => false, 
            ),
            'text'    => array(
                'add_upload_file_text' => __('Add Image', 'isystemscombr'), 
            ),
            'query_args' => array(
                'type' => array(
                    'image/jpeg',
                    'image/png',
                    //'image/svg+xml',
                ),
            ),
            'preview_size' => array(288, 100) 
        ) 
    );

    //Logo
    $cmb->add_field(
        array(
            'name'    => __('Logo', 'isystemscombr'),
            'desc'    => __('Format: SVG (recommended), PNG | Recommended size: 640x320px | Aspect ratio: 2:1', 'isystemscombr'),
            'id'      => $prefix .'logo',
            'type'    => 'file',
            'options' => array(
                'url' => false, 
            ),
            'text'    => array(
                'add_upload_file_text' => __('Add Image', 'isystemscombr'), 
            ),
            'query_args' => array(
                'type' => array(
                    //'image/jpeg',
                    'image/png',
                    'image/svg+xml',
                ),
            ),
            'preview_size' => array(240, 120) 
        ) 
    );

    /**
    * Headline
    */
    $cmb_headline = new_cmb2_box(
        array(
            'id'            => 'isystems_solution_headline_id',
            'title'         => __('Headline', 'isystemscombr'),
            'object_types'  => array('isystems_solution'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, 
        )
    );

    //Title
    $cmb_headline->add_field( 
        array(
            'name'       => __('Title', 'isystemscombr'),
            'desc'       => '',
            'id'         => $prefix . 'headline_title',
            'type'       => 'wysiwyg',
            'options' => array(
                
                'media_buttons' => false,
                'textarea_rows' => get_option('default_post_edit_rows', 2),
                'tabindex' => '',
                'editor_css' => '',
                'editor_class' => '',
                'teeny' => true,
                'tinymce' => true,
                'quicktags' => true
            ),
        )
    );

    //Desc
    $cmb_headline->add_field( 
        array(
            'name'       => __('Description', 'isystemscombr'),
            'desc'       => '',
            'id'         => $prefix . 'headline_desc',
            'type'       => 'wysiwyg',
            'options' => array(
                
                'media_buttons' => false,
                'textarea_rows' => get_option('default_post_edit_rows', 5),
                'tabindex' => '',
                'editor_css' => '',
                'editor_class' => '',
                'teeny' => true,
                'tinymce' => true,
                'quicktags' => true
            ),
        )
    );

    /**
    * Video
    */
    $cmb_video = new_cmb2_box(
        array(
            'id'            => 'isystems_solution_video_id',
            'title'         => __('Video', 'isystemscombr'),
            'object_types'  => array('isystems_solution'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, 
        )
    );

    //URL
    $cmb_video->add_field( 
        array(
            'name'       => __('Video (URL)', 'isystemscombr'),
            'desc'       => '',
            'id'         => $prefix . 'video_url',
            'type'       => 'text',
        )
    );

    /**
    * Industry Performance
    */
    $cmb_industry = new_cmb2_box(
        array(
            'id'            => 'isystems_solution_industry_id',
            'title'         => __('Industry Performance', 'isystemscombr'),
            'object_types'  => array('isystems_solution'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, 
        )
    );

    //Title
    $cmb_industry->add_field( 
        array(
            'name'       => __('Title', 'isystemscombr'),
            'desc'       => '',
            'id'         => $prefix . 'industry_title',
            'type'       => 'text',
        )
    );

    //Desc
    $cmb_industry->add_field( 
        array(
            'name'       => __('Description', 'isystemscombr'),
            'desc'       => '',
            'id'         => $prefix . 'industry_desc',
            'type'       => 'wysiwyg',
            'options' => array(
                
                'media_buttons' => false,
                'textarea_rows' => get_option('default_post_edit_rows', 3),
                'tabindex' => '',
                'editor_css' => '',
                'editor_class' => '',
                'teeny' => true,
                'tinymce' => true,
                'quicktags' => true
            ),
        )
    );

    //Industry Performance Group
    $industry_id = $cmb_industry->add_field(
        array(
            'id'          => $prefix . 'industry_sectors',
            'type'        => 'group',
            'description' => __('Industry Sectors', 'isystemscombr'),
            'options'     => array(
                'group_title'   => __('Sector {#}', 'isystemscombr'),
                'add_button'    => __('Add Another Sector', 'isystemscombr'),
                'remove_button' => __('Remove Sector', 'isystemscombr'),
                'sortable'      => true,
            ),
        )
    );

    //Sector
    $cmb_industry->add_group_field(
        $industry_id,
        array(
            'name'           => __('Sector', 'isystemscombr'),
            'desc'           => __('Select Industry Sector or leave blank to use Topic field', 'isystemscombr'),
            'id'             => 'sector',
            'type'           => 'select',
            'options'        => Get_Post_array(array('post_type' => 'isystems_sector'), false, true),
        )
    );

    //Topic
    $cmb_industry->add_group_field(
        $industry_id,
        array(
            'name'           => __('Topic', 'isystemscombr'),
            'desc'           => '',
            'id'             => 'topic',
            'type'           => 'text',
        )
    );

    //Icon Colored
    $cmb_industry->add_group_field(
        $industry_id,
        array(
            'name'    => __('Topic Icon (Colored)', 'isystemscombr'),
            'desc'    => __('Format: SVG (recommended), PNG | Recommended size: 320x320 | Aspect ratio: 1:1', 'isystemscombr'),
            'id'      => 'topic_icon_colored',
            'type'    => 'file',
            'options' => array(
                'url' => false, 
            ),
            'text'    => array(
                'add_upload_file_text' => __('Add Icon', 'isystemscombr'), 
            ),
            'query_args' => array(
                'type' => array(
                    //'image/jpeg',
                    'image/png',
                    'image/svg+xml',
                ),
            ),
            'preview_size' => array(64, 64) 
        ) 
    );

    //Icon White
    $cmb_industry->add_group_field(
        $industry_id,
        array(
            'name'    => __('Topic Icon (White)', 'isystemscombr'),
            'desc'    => __('Format: SVG (recommended), PNG | Recommended size: 320x320 | Aspect ratio: 1:1', 'isystemscombr'),
            'id'      => 'topic_icon_white',
            'type'    => 'file',
            'options' => array(
                'url' => false, 
            ),
            'text'    => array(
                'add_upload_file_text' => __('Add Icon', 'isystemscombr'), 
            ),
            'query_args' => array(
                'type' => array(
                    //'image/jpeg',
                    'image/png',
                    'image/svg+xml',
                ),
            ),
            'preview_size' => array(64, 64) 
        ) 
    );

    // Industry Title
    $cmb_industry->add_group_field(
        $industry_id,
        array (
            'name'       => __('Title', 'isystemscombr'),
            'desc'       => '',
            'id'         => 'title',
            'type'       => 'text',
        )
    );
    
    // Industry Description
    $cmb_industry->add_group_field(
        $industry_id,
        array (
            'name'       => __('Description', 'isystemscombr'),
            'desc'       => '',
            'id'         => 'content',
            'type'       => 'wysiwyg',
            'options' => array(
                
                'media_buttons' => false,
                'textarea_rows' => get_option('default_post_edit_rows', 5),
                'tabindex' => '',
                'editor_css' => '',
                'editor_class' => '',
                'teeny' => true,
                'tinymce' => true,
                'quicktags' => true
            ),
        )
    );

    /**
    * Benefits
    */
    $cmb_benefits = new_cmb2_box(
        array(
            'id'            => 'isystems_solution_benefits_id',
            'title'         => __('Benefits', 'isystemscombr'),
            'object_types'  => array('isystems_solution'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, 
        )
    );

    //Title
    $cmb_benefits->add_field( 
        array(
            'name'       => __('Title', 'isystemscombr'),
            'desc'       => '',
            'id'         => $prefix . 'benefits_title',
            'type'       => 'text',
        )
    );

    //Desc
    $cmb_benefits->add_field( 
        array(
            'name'       => __('Description', 'isystemscombr'),
            'desc'       => '',
            'id'         => $prefix . 'benefits_desc',
            'type'       => 'wysiwyg',
            'options' => array(
                
                'media_buttons' => false,
                'textarea_rows' => get_option('default_post_edit_rows', 3),
                'tabindex' => '',
                'editor_css' => '',
                'editor_class' => '',
                'teeny' => true,
                'tinymce' => true,
                'quicktags' => true
            ),
        )
    );

    //Benefits Topics Group
    $topics_id = $cmb_benefits->add_field(
        array(
            'id'          => $prefix . 'benefits_sectors',
            'type'        => 'group',
            'description' => __('Benefit', 'isystemscombr'),
            'options'     => array(
                'group_title'   => __('Benefit {#}', 'isystemscombr'),
                'add_button'    => __('Add Another Benefit', 'isystemscombr'),
                'remove_button' => __('Remove Benefit', 'isystemscombr'),
                'sortable'      => true,
            ),
        )
    );

    //Topic Icon
    $cmb_benefits->add_group_field(
        $topics_id,
        array (
            'name'    => __('Icon', 'isystemscombr'),
            'desc'    => __('Format: SVG (recommended), PNG | Recommended size: 320x320 | Aspect ratio: 1:1', 'isystemscombr'),
            'id'      => 'icon',
            'type'    => 'file',
            'options' => array(
                'url' => false, 
            ),
            'text'    => array(
                'add_upload_file_text' => __('Add Image', 'isystemscombr'), 
            ),
            'query_args' => array(
                'type' => array(
                    //'image/jpeg',
                    'image/png',
                    'image/svg+xml',
                ),
            ),
            'preview_size' => array(64, 64) 
        ) 
    );

    // Topic Title
    $cmb_benefits->add_group_field(
        $topics_id,
        array (
            'name'       => __('Title', 'isystemscombr'),
            'desc'       => '',
            'id'         => 'title',
            'type'       => 'text',
        )
    );

    /**
    * Testimonials
    */
    $cmb_testimonials = new_cmb2_box(
        array(
            'id'            => 'isystems_solution_testimonials_id',
            'title'         => __('Testimonials', 'isystemscombr'),
            'object_types'  => array('isystems_solution'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, 
        )
    );

    //Title
    $cmb_testimonials->add_field( 
        array(
            'name'       => __('Title', 'isystemscombr'),
            'desc'       => '',
            'id'         => $prefix . 'testimonials_title',
            'type'       => 'text',
        )
    );
    
    //Testimonials Group
    $testimonial_id = $cmb_testimonials->add_field(
        array(
            'id'          => $prefix . 'testimonials_testimonial',
            'type'        => 'group',
            'description' => '',
            'options'     => array(
                'group_title'   => __('Testimonial {#}', 'isystemscombr'),
                'add_button'    => __('Add Another Testimonial', 'isystemscombr'),
                'remove_button' => __('Remove Testimonial', 'isystemscombr'),
                'sortable'      => true,
            ),
        )
    );

    // Testimonial Client
    $cmb_testimonials->add_group_field(
        $testimonial_id,
        array (
            'name'       => __('Client', 'isystemscombr'),
            'desc'       => '',
            'id'         => 'name',
            'type'       => 'text',
        )
    );

    // Testimonial Role
    $cmb_testimonials->add_group_field(
        $testimonial_id,
        array (
            'name'       => __('Role', 'isystemscombr'),
            'desc'       => '',
            'id'         => 'role',
            'type'       => 'text',
        )
    );

    // Testimonial Description
    $cmb_testimonials->add_group_field(
        $testimonial_id,
        array (
            'name'       => __('Testimonial', 'isystemscombr'),
            'desc'       => '',
            'id'         => 'content',
            'type'       => 'wysiwyg',
                'options' => array(
                    'media_buttons' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 5),
                    'tabindex' => '',
                    'editor_css' => '',
                    'editor_class' => '',
                    'teeny' => true,
                    'tinymce' => true,
                    'quicktags' => true
                ),
        )
    );

    /**
    * Cases
    */
    $cmb_cases = new_cmb2_box(
        array(
            'id'            => 'isystems_solution_cases_id',
            'title'         => __('Cases', 'isystemscombr'),
            'object_types'  => array('isystems_solution'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, 
        )
    );

    //Title
    $cmb_cases->add_field( 
        array(
            'name'       => __('Title', 'isystemscombr'),
            'desc'       => '',
            'id'         => $prefix . 'cases_title', 
            'type'       => 'text',
        )
    );
    
    //Case1
    $cmb_cases->add_field(
        array(
            'name'           => __('Case 1', 'isystemscombr'),
            'desc'           => __('Select a case to display', 'isystemscombr'),
            'id'             => $prefix.'cases_case_1',
            'type'           => 'select',
            'options'        => Get_Post_array(array('post_type' => 'isystems_case'), false, true),
        )
    );

    //Case2
    $cmb_cases->add_field(
        array(
            'name'           => __('Case 2', 'isystemscombr'),
            'desc'           => __('Select a case to display', 'isystemscombr'),
            'id'             => $prefix.'cases_case_2',
            'type'           => 'select',
            'options'        => Get_Post_array(array('post_type' => 'isystems_case'), false, true),
        )
    );

    //Case3
    $cmb_cases->add_field(
        array(
            'name'           => __('Case 3', 'isystemscombr'),
            'desc'           => __('Select a case to display', 'isystemscombr'),
            'id'             => $prefix.'cases_case_3',
            'type'           => 'select',
            'options'        => Get_Post_array(array('post_type' => 'isystems_case'), false, true),
        )
    );

    /**
    * Form
    */
    $cmb_form = new_cmb2_box(
        array(
            'id'            => 'isystems_solution_form_id',
            'title'         => __('Form', 'isystemscombr'),
            'object_types'  => array('isystems_solution'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, 
        )
    );

    //Title
    $cmb_form->add_field( 
        array(
            'name'       => __('Title', 'isystemscombr'),
            'desc'       => '',
            'id'         => $prefix . 'form_title',
            'type'       => 'text',
        )
    );

    //Desc
    $cmb_form->add_field( 
        array(
            'name'       => __('Description', 'isystemscombr'),
            'desc'       => '',
            'id'         => $prefix . 'form_desc',
            'type'       => 'wysiwyg',
            'options' => array(
                'media_buttons' => false,
                'textarea_rows' => get_option('default_post_edit_rows', 5),
                'tabindex' => '',
                'editor_css' => '',
                'editor_class' => '',
                'teeny' => true,
                'tinymce' => true,
                'quicktags' => true
            ),
        )
    );

    //Shortcode
    $cmb_form->add_field( 
        array(
            'name'       => __('Form Shortcode', 'isystemscombr'),
            'desc'       => 'Contact Form 7 Shortcode',
            'id'         => $prefix . 'form_shortcode',
            'type'       => 'text',
        )
    );

    /**
    * Banner Related Solution
    */
    $cmb_banner_related = new_cmb2_box(
        array(
            'id'            => 'isystems_solution_banner_related_id',
            'title'         => __('Banner Related Solution', 'isystemscombr'),
            'object_types'  => array('isystems_solution'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, 
        )
    );

    //Solution
    $cmb_banner_related->add_field(
        array(
            'name'           => __('Solution', 'isystemscombr'),
            'desc'           => '',
            'id'             => $prefix.'banner_related_solution',
            'type'           => 'select',
            'options'        => Get_Post_array(array('post_type' => 'isystems_solution')),
        )
    );

    //CTA
    $cmb_banner_related->add_field( 
        array(
            'name'       => __('CTA', 'isystemscombr'),
            'desc'       => __('Call for related solution', 'isystemscombr'),
            'id'         => $prefix . 'banner_related_cta',
            'type'       => 'text',
        )
    );

    //Button Text
    $cmb_banner_related->add_field(
        array(
            'name'       => __('Button Text', 'isystemscombr'),
            'desc'       => __('Button call', 'isystemscombr'),
            'id'         => $prefix . 'banner_related_btn_text',
            'type'       => 'text',
        )
    );
}
add_action('cmb2_admin_init', 'cmb_isystems_solution');

/**** 
 * Sectors
 */
function cmb_isystems_sector() 
{
    $prefix = '_isystems_sector_';
    
    /**
    * Initiate the metabox
    */
    $cmb = new_cmb2_box(
        array(
            'id'            => 'isystems_sector_id',
            'title'         => __('Sector', 'isystemscombr'),
            'object_types'  => array('isystems_sector'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, 
        )
    );

    //Icon Colored
    $cmb->add_field(
        array(
            'name'    => __('Icon (Colored)', 'isystemscombr'),
            'desc'    => __('Format: SVG (recommended), PNG | Recommended size: 320x320 | Aspect ratio: 1:1', 'isystemscombr'),
            'id'      => $prefix .'icon_colored',
            'type'    => 'file',
            'options' => array(
                'url' => false, 
            ),
            'text'    => array(
                'add_upload_file_text' => __('Add Icon', 'isystemscombr'), 
            ),
            'query_args' => array(
                'type' => array(
                    //'image/jpeg',
                    'image/png',
                    'image/svg+xml',
                ),
            ),
            'preview_size' => array(64, 64) 
        ) 
    );

    //Icon White
    $cmb->add_field(
        array(
            'name'    => __('Icon (White)', 'isystemscombr'),
            'desc'    => __('Format: SVG (recommended), PNG | Recommended size: 320x320 | Aspect ratio: 1:1', 'isystemscombr'),
            'id'      => $prefix .'icon_white',
            'type'    => 'file',
            'options' => array(
                'url' => false, 
            ),
            'text'    => array(
                'add_upload_file_text' => __('Add Icon', 'isystemscombr'), 
            ),
            'query_args' => array(
                'type' => array(
                    //'image/jpeg',
                    'image/png',
                    'image/svg+xml',
                ),
            ),
            'preview_size' => array(64, 64) 
        ) 
    );
}
add_action('cmb2_admin_init', 'cmb_isystems_sector');

/***********************************************************************************
 * Page Fields
 * ********************************************************************************/
/**
 * Front-page
 */
add_action(
    'cmb2_admin_init',
    function () {
        $prefix = '_isystems_frontpage_';

        /**
        * Featured Banner / Hero
        */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => 'isystems_frontpage_hero_id',
                'title'         => __('Featured Banner', 'isystemscombr'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on_cb' => 'Show_On_Front_page',
            )
        );

        //Hero Group
        $hero_id = $cmb_hero->add_field(
            array(
                'id'          => $prefix . 'hero_slides',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   =>__('Slide {#}', 'isystemscombr'),
                    'add_button'   =>__('Add Another Slide', 'isystemscombr'),
                    'remove_button' =>__('Remove Slide', 'isystemscombr'),
                    'sortable'      => true,
                ),
            )
        );

        //Hero Background Image
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image', 'isystemscombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 2880x1300px | Aspect ratio: 144:65', 'isystemscombr'),
                'id'          => 'bkg_img',
                'type'        => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'isystemscombr'),
                ),
                'query_args' => array(
                    'type' => array(
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(288, 130)
            )
        );

        //Hero Background Video
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Background Video', 'isystemscombr'),
                'desc'       => __('Format: MP4 (muted)', 'isystemscombr'),
                'id'         => 'bkg_video',
                'type'        => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Video', 'isystemscombr'),
                ),
                'query_args' => array(
                    'type' => array(
                        'video/mp4',
                    ),
                ),
                'preview_size' => array(288, 130)
            )
        );

        //Hero Title
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Title', 'isystemscombr'),
                'desc'       => '',
                'id'         => 'title',
                'type'       => 'text',
            )
        );

        //Hero Button Text
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Button Text', 'isystemscombr'),
                'desc'       => __('Button call', 'isystemscombr'),
                'id'         => 'btn_text',
                'type'       => 'text',
            )
        );

        //Hero Button Target
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Button Target', 'isystemscombr'),
                'desc'       => __('Select button target', 'isystemscombr'),
                'id'         => 'btn_target',
                'type'       => 'radio',
                'show_option_none' => false,
                'options'          => array(
                    'page'   => __('Page', 'isystemscombr'),
                    'video_url'  => __('Video Modal', 'isystemscombr'),
                    'manifest'  => __('Manifest Modal', 'isystemscombr'),
                ),
                'default' => 'page',
            )
        );

        //Hero Button Target Page
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Page Target', 'isystemscombr'),
                'desc'       => '',
                'id'         => 'target_page_id',
                'type'       => 'select',
                'options'    => Get_Post_array(array('post_type' => 'page'), true),
            )
        );

        //Hero Button Link
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Modal Video (URL)', 'isystemscombr'),
                'desc'       => '',
                'id'         => 'target_video_url',
                'type'       => 'text',
            )
        );

        /**
        * Who we are
        */
        $cmb_whoweare = new_cmb2_box(
            array(
                'id'            => 'isystems_frontpage_whoweare_id',
                'title'         => __('Who we are', 'isystemscombr'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on_cb' => 'Show_On_Front_page',
            )
        );

        //Title
        $cmb_whoweare->add_field(
            array(
                'name'       => __('Title', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'whoweare_title',
                'type'       => 'wysiwyg',
                'options' => array(
                    'media_buttons' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 5),
                    'tabindex' => '',
                    'editor_css' => '',
                    'editor_class' => '',
                    'teeny' => true,
                    'tinymce' => true,
                    'quicktags' => true
                ),
            )
        );

        //Description
        $cmb_whoweare->add_field(
            array(
                'name'       => __('Description', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'whoweare_desc',
                'type'       => 'wysiwyg',
                'options' => array(
                    'media_buttons' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 5),
                    'tabindex' => '',
                    'editor_css' => '',
                    'editor_class' => '',
                    'teeny' => true,
                    'tinymce' => true,
                    'quicktags' => true
                ),
            )
        );

        //Button Text
        $cmb_whoweare->add_field(
            array(
                'name'       => __('Button Text', 'isystemscombr'),
                'desc'       => __('Button call', 'isystemscombr'),
                'id'         => $prefix . 'whoweare_btn_text',
                'type'       => 'text',
            )
        );

        //Page Target
        $cmb_whoweare->add_field(
            array(
                'name'       => __('Page Target', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'whoweare_btn_page_id',
                'type'       => 'select',
                'options'    => Get_Post_array(array('post_type' => 'page'), true),
            )
        );

        /**
        * Clients
        */
        $cmb_clients = new_cmb2_box(
            array(
                'id'            => 'isystems_frontpage_clients_id',
                'title'         => __('Clients', 'isystemscombr'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on_cb' => 'Show_On_Front_page',
            )
        );

        //Title
        $cmb_clients->add_field(
            array(
                'name'       => __('Title', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'clients_title',
                'type'       => 'text',
            )
        );

        //Galley
        $cmb_clients->add_field( 
            array(
                'name'       => __('Clients Carousel (Logo)', 'isystemscombr'),
                'desc'    => __('Format: SVG (recommended), PNG | Recommended size: 640x320px | Aspect ratio: 2:1', 'isystemscombr'),
                'id'         => $prefix . 'clients_carousel',
                'type'       => 'file_list',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' => __('Add Images', 'isystemscombr'), 
                ),
                'query_args' => array(
                    'type' => array(
                        //'image/jpeg',
                        'image/png',
                        'image/svg+xml',
                    ),
                ),
                'preview_size' => array(240, 120) 
            )
        );

        /**
        * Solutions
        */
        $cmb_solutions = new_cmb2_box(
            array(
                'id'            => 'isystems_frontpage_solutions_id',
                'title'         => __('Solutions', 'isystemscombr'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on_cb' => 'Show_On_Front_page',
            )
        );

        //Background Image
        $cmb_solutions->add_field(
            array(
                'name'    => __('Background Image', 'isystemscombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 2880x1000px | Aspect ratio: 72:25', 'isystemscombr'),
                'id'      => $prefix .'solutions_bkg_img',
                'type'    => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' => __('Add Image', 'isystemscombr'), 
                ),
                'query_args' => array(
                    'type' => array(
                        'image/jpeg',
                        'image/png',
                        //'image/svg+xml',
                    ),
                ),
                'preview_size' => array(288, 100) 
            ) 
        );

        //Title
        $cmb_solutions->add_field(
            array(
                'name'       => __('Title', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'solutions_title',
                'type'       => 'text',
            )
        );

        //Description
        $cmb_solutions->add_field(
            array(
                'name'       => __('Description', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'solutions_desc',
                'type'       => 'wysiwyg',
                'options' => array(
                    'media_buttons' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 5),
                    'tabindex' => '',
                    'editor_css' => '',
                    'editor_class' => '',
                    'teeny' => true,
                    'tinymce' => true,
                    'quicktags' => true
                ),
            )
        );

        //Button Text
        $cmb_solutions->add_field(
            array(
                'name'       => __('Button Text', 'isystemscombr'),
                'desc'       => __('Button call', 'isystemscombr'),
                'id'         => $prefix . 'solutions_btn_text',
                'type'       => 'text',
            )
        );

        //Page Target
        $cmb_solutions->add_field(
            array(
                'name'       => __('Page Target', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'solutions_btn_page_id',
                'type'       => 'select',
                'options'    => Get_Post_array(array('post_type' => 'page'), true),
            )
        );

        /**
        * Company Numbers
        */
        $cmb_numbers = new_cmb2_box(
            array(
                'id'            => 'isystems_frontpage_numbers_id',
                'title'         => __('Company Numbers', 'isystemscombr'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on_cb' => 'Show_On_Front_page',
            )
        );

        //Title
        $cmb_numbers->add_field(
            array(
                'name'       => __('Title', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'numbers_title',
                'type'       => 'text',
            )
        );

        //Box 1 Icon
        $cmb_numbers->add_field(
            array(
                'name'    => __('Box 1 Icon', 'isystemscombr'),
                'desc'    => __('Format: SVG (recommended), PNG | Recommended size: 320x320px | Aspect ratio: 1:1', 'isystemscombr'),
                'id'      => $prefix .'numbers_box1_icon',
                'type'    => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' => __('Add Image', 'isystemscombr'), 
                ),
                'query_args' => array(
                    'type' => array(
                        //'image/jpeg',
                        'image/png',
                        'image/svg+xml',
                    ),
                ),
                'preview_size' => array(64, 64) 
            ) 
        );

        //Box 1 Title
        $cmb_numbers->add_field(
            array(
                'name'       => __('Box 1 Title', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'numbers_box1_title',
                'type'       => 'text',
            )
        );

        //Box 1 Description
        $cmb_numbers->add_field(
            array(
                'name'       => __('Box 1 Description', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'numbers_box1_desc',
                'type'       => 'wysiwyg',
                'options' => array(
                    'media_buttons' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 5),
                    'tabindex' => '',
                    'editor_css' => '',
                    'editor_class' => '',
                    'teeny' => true,
                    'tinymce' => true,
                    'quicktags' => true
                ),
            )
        );

        //Box 2 Icon
        $cmb_numbers->add_field(
            array(
                'name'    => __('Box 2 Icon', 'isystemscombr'),
                'desc'    => __('Format: SVG (recommended), PNG | Recommended size: 320x320px | Aspect ratio: 1:1', 'isystemscombr'),
                'id'      => $prefix .'numbers_box2_icon',
                'type'    => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' => __('Add Image', 'isystemscombr'), 
                ),
                'query_args' => array(
                    'type' => array(
                        //'image/jpeg',
                        'image/png',
                        'image/svg+xml',
                    ),
                ),
                'preview_size' => array(64, 64) 
            ) 
        );

        //Box 2 Title
        $cmb_numbers->add_field(
            array(
                'name'       => __('Box 2 Title', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'numbers_box2_title',
                'type'       => 'text',
            )
        );

        //Box 2 Description
        $cmb_numbers->add_field(
            array(
                'name'       => __('Box 2 Description', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'numbers_box2_desc',
                'type'       => 'wysiwyg',
                'options' => array(
                    'media_buttons' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 5),
                    'tabindex' => '',
                    'editor_css' => '',
                    'editor_class' => '',
                    'teeny' => true,
                    'tinymce' => true,
                    'quicktags' => true
                ),
            )
        );

        //Box 3 Icon
        $cmb_numbers->add_field(
            array(
                'name'    => __('Box 3 Icon', 'isystemscombr'),
                'desc'    => __('Format: SVG (recommended), PNG | Recommended size: 320x320px | Aspect ratio: 1:1', 'isystemscombr'),
                'id'      => $prefix .'numbers_box3_icon',
                'type'    => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' => __('Add Image', 'isystemscombr'), 
                ),
                'query_args' => array(
                    'type' => array(
                        //'image/jpeg',
                        'image/png',
                        'image/svg+xml',
                    ),
                ),
                'preview_size' => array(64, 64) 
            ) 
        );

        //Box 3 Title
        $cmb_numbers->add_field(
            array(
                'name'       => __('Box 3 Title', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'numbers_box3_title',
                'type'       => 'text',
            )
        );

        //Box 3 Description
        $cmb_numbers->add_field(
            array(
                'name'       => __('Box 3 Description', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'numbers_box3_desc',
                'type'       => 'wysiwyg',
                'options' => array(
                    'media_buttons' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 5),
                    'tabindex' => '',
                    'editor_css' => '',
                    'editor_class' => '',
                    'teeny' => true,
                    'tinymce' => true,
                    'quicktags' => true
                ),
            )
        );

        /**
        * Cases
        */
        $cmb_cases = new_cmb2_box(
            array(
                'id'            => 'isystems_frontpage_cases_id',
                'title'         => __('Cases', 'isystemscombr'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on_cb' => 'Show_On_Front_page',
            )
        );

        //Title
        $cmb_cases->add_field(
            array(
                'name'       => __('Title', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'cases_title',
                'type'       => 'text',
            )
        );

        //Button Text
        $cmb_cases->add_field(
            array(
                'name'       => __('Button Text', 'isystemscombr'),
                'desc'       => __('Button call', 'isystemscombr'),
                'id'         => $prefix . 'cases_btn_text',
                'type'       => 'text',
            )
        );

        //Page Target
        $cmb_cases->add_field(
            array(
                'name'       => __('Page Target', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'cases_btn_page_id',
                'type'       => 'select',
                'options'    => Get_Post_array(array('post_type' => 'page'), true),
            )
        );

        /**
        * Career
        */
        $cmb_career = new_cmb2_box(
            array(
                'id'            => 'isystems_frontpage_career_id',
                'title'         => __('Career', 'isystemscombr'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on_cb' => 'Show_On_Front_page',
            )
        );

        //Featured Image
        $cmb_career->add_field(
            array(
                'name'    => __('Featured Image', 'isystemscombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 1440x1392px | Aspect ratio: 30:29', 'isystemscombr'),
                'id'      => $prefix .'career_img',
                'type'    => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' => __('Add Image', 'isystemscombr'), 
                ),
                'query_args' => array(
                    'type' => array(
                        'image/jpeg',
                        'image/png',
                        //'image/svg+xml',
                    ),
                ),
                'preview_size' => array(144, 139) 
            )
        ); 

        //Title
        $cmb_career->add_field(
            array(
                'name'       => __('Title', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'career_title',
                'type'       => 'text',
            )
        );

        //Description
        $cmb_career->add_field(
            array(
                'name'       => __('Description', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'career_desc',
                'type'       => 'wysiwyg',
                'options' => array(
                    'media_buttons' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 5),
                    'tabindex' => '',
                    'editor_css' => '',
                    'editor_class' => '',
                    'teeny' => true,
                    'tinymce' => true,
                    'quicktags' => true
                ),
            )
        );

        //Button Text
        $cmb_career->add_field(
            array(
                'name'       => __('Button Text', 'isystemscombr'),
                'desc'       => __('Button call', 'isystemscombr'),
                'id'         => $prefix . 'career_btn_text',
                'type'       => 'text',
            )
        );

        //Page Target
        $cmb_career->add_field(
            array(
                'name'       => __('Page Target', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'career_btn_page_id',
                'type'       => 'select',
                'options'    => Get_Post_array(array('post_type' => 'page'), true),
            )
        );
    }
);

/**
 * Who We Are
 */
add_action(
    'cmb2_admin_init',
    function () {
        $prefix = '_isystems_whoweare_';

        /**
        * Header
        */
        $cmb_header = new_cmb2_box(
            array(
                'id'            => 'isystems_whoweare_header_id',
                'title'         => __('Header', 'isystemscombr'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-quem-somos.blade.php'),
            )
        );

        //Featured Image
        $cmb_header->add_field(
            array(
                'name'    => __('Featured Image', 'isystemscombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 1440x800px | Aspect ratio: 9:5', 'isystemscombr'),
                'id'      => $prefix .'header_img',
                'type'    => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' => __('Add Image', 'isystemscombr'), 
                ),
                'query_args' => array(
                    'type' => array(
                        'image/jpeg',
                        'image/png',
                        //'image/svg+xml',
                    ),
                ),
                'preview_size' => array(144, 80) 
            )
        ); 

        //Title
        $cmb_header->add_field(
            array(
                'name'       => __('Title', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'header_title',
                'type'       => 'text',
            )
        );

        //Description
        $cmb_header->add_field(
            array(
                'name'       => __('Description', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'header_desc',
                'type'       => 'wysiwyg',
                'options' => array(
                    'media_buttons' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 5),
                    'tabindex' => '',
                    'editor_css' => '',
                    'editor_class' => '',
                    'teeny' => true,
                    'tinymce' => true,
                    'quicktags' => true
                ),
            )
        );

        /**
        * History
        */
        $cmb_history = new_cmb2_box(
            array(
                'id'            => 'isystems_whoweare_history_id',
                'title'         => __('History', 'isystemscombr'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-quem-somos.blade.php'),
            )
        );

        //Title
        $cmb_history->add_field(
            array(
                'name'       => __('Title', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'history_title',
                'type'       => 'text',
            )
        );

        //Description
        $cmb_history->add_field(
            array(
                'name'       => __('Description', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'history_desc',
                'type'       => 'wysiwyg',
                'options' => array(
                    'media_buttons' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 5),
                    'tabindex' => '',
                    'editor_css' => '',
                    'editor_class' => '',
                    'teeny' => true,
                    'tinymce' => true,
                    'quicktags' => true
                ),
            )
        );

        /**
        * Manifest
        */
        $cmb_manifest = new_cmb2_box(
            array(
                'id'            => 'isystems_whoweare_manifest_id',
                'title'         => __('Manifest', 'isystemscombr'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-quem-somos.blade.php'),
            )
        );

        //Background Image
        $cmb_manifest->add_field(
            array(
                'name'    => __('Background Image', 'isystemscombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 2880x1000px | Aspect ratio: 72:25', 'isystemscombr'),
                'id'      => $prefix .'manifest_bkg_img',
                'type'    => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' => __('Add Image', 'isystemscombr'), 
                ),
                'query_args' => array(
                    'type' => array(
                        'image/jpeg',
                        'image/png',
                        //'image/svg+xml',
                    ),
                ),
                'preview_size' => array(288, 100) 
            ) 
        );

        //Background Video
        $cmb_manifest->add_field(
            array(
                'name'       => __('Background Video', 'isystemscombr'),
                'desc'       => __('Format: MP4 (muted)', 'isystemscombr'),
                'id'         => $prefix . 'manifest_bkg_video',
                'type'        => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Video', 'isystemscombr'),
                ),
                //'query_args' => array(
                //    'type' => array(
                //        'video/mp4',
                //    ),
                //),
                'preview_size' => array(288, 100)
            )
        );

        //Title
        $cmb_manifest->add_field(
            array(
                'name'       => __('Title', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'manifest_title',
                'type'       => 'text',
            )
        );

        //Description
        $cmb_manifest->add_field(
            array(
                'name'       => __('Description', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'manifest_desc',
                'type'       => 'text',
            )
        );

        //Button Text
        $cmb_manifest->add_field(
            array(
                'name'       => __('Button Text', 'isystemscombr'),
                'desc'       => __('Button call', 'isystemscombr'),
                'id'         => $prefix . 'manifest_btn_text',
                'type'       => 'text',
            )
        );

        //Button Target
        $cmb_manifest->add_field(
            array(
                'name'       => __('Button Target', 'isystemscombr'),
                'desc'       => __('Select button target', 'isystemscombr'),
                'id'         => $prefix .  'manifest_btn_target',
                'type'       => 'radio',
                'show_option_none' => false,
                'options'          => array(
                    'video_url'  => __('Video Modal', 'isystemscombr'),
                    'manifest'  => __('Manifest Modal', 'isystemscombr'),
                ),
                'default' => 'video_url',
            )
        );

        //Button Link
        $cmb_manifest->add_field(                
            array(
                'name'       => __('Video (URL)', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'manifest_btn_video_url',
                'type'       => 'text',
            )
        );

        /**
        * Acknowledgment
        */
        $cmb_acknowledgment = new_cmb2_box(
            array(
                'id'            => 'isystems_whoweare_acknowledgment_id',
                'title'         => __('Acknowledgment', 'isystemscombr'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-quem-somos.blade.php'),
            )
        );

        //Title
        $cmb_acknowledgment->add_field(
            array(
                'name'       => __('Title', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'acknowledgment_title',
                'type'       => 'text',
            )
        );

        //Description
        $cmb_acknowledgment->add_field(
            array(
                'name'       => __('Description', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'acknowledgment_desc',
                'type'       => 'wysiwyg',
                'options' => array(
                    'media_buttons' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 5),
                    'tabindex' => '',
                    'editor_css' => '',
                    'editor_class' => '',
                    'teeny' => true,
                    'tinymce' => true,
                    'quicktags' => true
                ),
            )
        );

        //Featured Image
        $cmb_acknowledgment->add_field(
            array(
                'name'    => __('Featured Image', 'isystemscombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 1440x800px | Aspect ratio: 9:5', 'isystemscombr'),
                'id'      => $prefix .'acknowledgment_img',
                'type'    => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' => __('Add Image', 'isystemscombr'), 
                ),
                'query_args' => array(
                    'type' => array(
                        'image/jpeg',
                        'image/png',
                        //'image/svg+xml',
                    ),
                ),
                'preview_size' => array(144, 80) 
            )
        ); 

        /**
        * Awards
        */
        $cmb_awards = new_cmb2_box(
            array(
                'id'            => 'isystems_whoweare_awards_id',
                'title'         => __('Awards', 'isystemscombr'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-quem-somos.blade.php'),
            )
        );

        //Show
        $cmb_awards->add_field(
            array(
                'name'       => __('Show Awards', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'awards_show',
                'type'       => 'checkbox',
            )
        );

        //Title
        $cmb_awards->add_field(
            array(
                'name'       => __('Title', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'awards_title',
                'type'       => 'text',
            )
        );

        //Awards Group
        $awards_id = $cmb_awards->add_field(
            array(
                'id'          => $prefix . 'headline_awards',
                'type'        => 'group',
                'description' => __('Awards', 'isystemscombr'),
                'options'     => array(
                    'group_title'   => __('Award {#}', 'isystemscombr'),
                    'add_button'    => __('Add Another Award', 'isystemscombr'),
                    'remove_button' => __('Remove Award', 'isystemscombr'),
                    'sortable'      => true,
                ),
            )
        );

        //Award Icon
        $cmb_awards->add_group_field(
            $awards_id,
            array (
                'name'    => __('Seal', 'isystemscombr'),
                'desc'    => __('Format: SVG (recommended), PNG | Recommended size: 320x320px | Aspect ratio: 1:1', 'isystemscombr'),
                'id'      => 'seal',
                'type'    => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' => __('Add Image', 'isystemscombr'), 
                ),
                'query_args' => array(
                    'type' => array(
                        //'image/jpeg',
                        'image/png',
                        'image/svg+xml',
                    ),
                ),
                'preview_size' => array(96, 96) 
            ) 
        );

        //Award Name
        $cmb_awards->add_group_field(
            $awards_id,
            array (
                'name'       => __('Name', 'isystemscombr'),
                'desc'       => '',
                'id'         => 'name',
                'type'       => 'text',
            )
        );
        
        //Award Year
        $cmb_awards->add_group_field(
            $awards_id,
            array (
                'name'       => __('Year', 'isystemscombr'),
                'desc'       => '',
                'id'         => 'year',
                'type'       => 'text',
                'attributes' => array(
                    'type' => 'number',
                    'pattern' => '\d*',
                ),
            )
        );

        /**
        * Career
        */
        $cmb_career = new_cmb2_box(
            array(
                'id'            => 'isystems_whoweare_career_id',
                'title'         => __('Career', 'isystemscombr'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-quem-somos.blade.php'),
            )
        );

        //Featured Image
        $cmb_career->add_field(
            array(
                'name'    => __('Featured Image', 'isystemscombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 1440x1392px | Aspect ratio: 30:29', 'isystemscombr'),
                'id'      => $prefix .'career_img',
                'type'    => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' => __('Add Image', 'isystemscombr'), 
                ),
                'query_args' => array(
                    'type' => array(
                        'image/jpeg',
                        'image/png',
                        //'image/svg+xml',
                    ),
                ),
                'preview_size' => array(144, 139) 
            )
        ); 

        //Title
        $cmb_career->add_field(
            array(
                'name'       => __('Title', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'career_title',
                'type'       => 'text',
            )
        );

        //Description
        $cmb_career->add_field(
            array(
                'name'       => __('Description', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'career_desc',
                'type'       => 'wysiwyg',
                'options' => array(
                    'media_buttons' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 5),
                    'tabindex' => '',
                    'editor_css' => '',
                    'editor_class' => '',
                    'teeny' => true,
                    'tinymce' => true,
                    'quicktags' => true
                ),
            )
        );

        //Button Text
        $cmb_career->add_field(
            array(
                'name'       => __('Button Text', 'isystemscombr'),
                'desc'       => __('Button call', 'isystemscombr'),
                'id'         => $prefix . 'career_btn_text',
                'type'       => 'text',
            )
        );

        //Page Target
        $cmb_career->add_field(
            array(
                'name'       => __('Page Target', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'career_btn_page_id',
                'type'       => 'select',
                'options'    => Get_Post_array(array('post_type' => 'page'), true),
            )
        );
    }
);

/**
 * Solutions
 */
add_action(
    'cmb2_admin_init',
    function () {
        $prefix = '_isystems_solutions_';

        /**
        * Header
        */
        $cmb_header = new_cmb2_box(
            array(
                'id'            => 'isystems_solutions_header_id',
                'title'         => __('Header', 'isystemscombr'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-solucoes.blade.php'),
            )
        );

        //Featured Image
        $cmb_header->add_field(
            array(
                'name'    => __('Featured Image', 'isystemscombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 1440x1152px | Aspect ratio: 5:4', 'isystemscombr'),
                'id'      => $prefix .'header_img',
                'type'    => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' => __('Add Image', 'isystemscombr'), 
                ),
                'query_args' => array(
                    'type' => array(
                        'image/jpeg',
                        'image/png',
                        //'image/svg+xml',
                    ),
                ),
                'preview_size' => array(144, 115) 
            )
        ); 

        //Title
        $cmb_header->add_field(
            array(
                'name'       => __('Title', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'header_title',
                'type'       => 'text',
            )
        );

        //Description
        $cmb_header->add_field(
            array(
                'name'       => __('Description', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'header_desc',
                'type'       => 'wysiwyg',
                'options' => array(
                    'media_buttons' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 5),
                    'tabindex' => '',
                    'editor_css' => '',
                    'editor_class' => '',
                    'teeny' => true,
                    'tinymce' => true,
                    'quicktags' => true
                ),
            )
        );

        /**
        * Industry Sectors
        */
        $cmb_sectors = new_cmb2_box(
            array(
                'id'            => 'isystems_solutions_sectors_id',
                'title'         => __('Industry Sectors', 'isystemscombr'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-solucoes.blade.php'),
            )
        );

        //Title
        $cmb_sectors->add_field(
            array(
                'name'       => __('Title', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'sectors_title',
                'type'       => 'text',
            )
        );

        //Description
        $cmb_sectors->add_field(
            array(
                'name'       => __('Description', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'sectors_desc',
                'type'       => 'wysiwyg',
                'options' => array(
                    'media_buttons' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 5),
                    'tabindex' => '',
                    'editor_css' => '',
                    'editor_class' => '',
                    'teeny' => true,
                    'tinymce' => true,
                    'quicktags' => true
                ),
            )
        );

        /**
        * Our Solutions
        */
        $cmb_oursolutions = new_cmb2_box(
            array(
                'id'            => 'isystems_oursolutions_oursolutions_id',
                'title'         => __('Our Solutions', 'isystemscombr'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-solucoes.blade.php'),
            )
        );

        //Title
        $cmb_oursolutions->add_field(
            array(
                'name'       => __('Title', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'oursolutions_title',
                'type'       => 'text',
            )
        );
    }
);

/**
 * Cases
 */
add_action(
    'cmb2_admin_init',
    function () {
        $prefix = '_isystems_cases_';

        /**
        * Header
        */
        $cmb_header = new_cmb2_box(
            array(
                'id'            => 'isystems_cases_header_id',
                'title'         => __('Header', 'isystemscombr'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-cases.blade.php'),
            )
        );

        //Featured Image
        $cmb_header->add_field(
            array(
                'name'    => __('Featured Image', 'isystemscombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 1440x1000px | Aspect ratio: 36:25', 'isystemscombr'),
                'id'      => $prefix .'header_img',
                'type'    => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' => __('Add Image', 'isystemscombr'), 
                ),
                'query_args' => array(
                    'type' => array(
                        'image/jpeg',
                        'image/png',
                        //'image/svg+xml',
                    ),
                ),
                'preview_size' => array(144, 100) 
            )
        ); 

        //Title
        $cmb_header->add_field(
            array(
                'name'       => __('Title', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'header_title',
                'type'       => 'text',
            )
        );

        //Description
        $cmb_header->add_field(
            array(
                'name'       => __('Description', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'header_desc',
                'type'       => 'wysiwyg',
                'options' => array(
                    'media_buttons' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 5),
                    'tabindex' => '',
                    'editor_css' => '',
                    'editor_class' => '',
                    'teeny' => true,
                    'tinymce' => true,
                    'quicktags' => true
                ),
            )
        );

        /**
        * Testimonials
        */
        $cmb_testimonials = new_cmb2_box(
            array(
                'id'            => 'isystems_cases_testimonials_id',
                'title'         => __('Testimonials', 'isystemscombr'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-cases.blade.php'),
            )
        );

        //Title
        $cmb_testimonials->add_field( 
            array(
                'name'       => __('Title', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'testimonials_title',
                'type'       => 'text',
            )
        );
        
        //Testimonials Group
        $testimonial_id = $cmb_testimonials->add_field(
            array(
                'id'          => $prefix . 'testimonials_testimonial',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   => __('Testimonial {#}', 'isystemscombr'),
                    'add_button'    => __('Add Another Testimonial', 'isystemscombr'),
                    'remove_button' => __('Remove Testimonial', 'isystemscombr'),
                    'sortable'      => true,
                ),
            )
        );

        // Testimonial Client
        $cmb_testimonials->add_group_field(
            $testimonial_id,
            array (
                'name'       => __('Client', 'isystemscombr'),
                'desc'       => '',
                'id'         => 'name',
                'type'       => 'text',
            )
        );

        // Testimonial Role
        $cmb_testimonials->add_group_field(
            $testimonial_id,
            array (
                'name'       => __('Role', 'isystemscombr'),
                'desc'       => '',
                'id'         => 'role',
                'type'       => 'text',
            )
        );

        //Testimonial Description
        $cmb_testimonials->add_group_field(
            $testimonial_id,
            array (
                'name'       => __('Testimonial', 'isystemscombr'),
                'desc'       => '',
                'id'         => 'content',
                'type'       => 'wysiwyg',
                'options' => array(
                    'media_buttons' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 5),
                    'tabindex' => '',
                    'editor_css' => '',
                    'editor_class' => '',
                    'teeny' => true,
                    'tinymce' => true,
                    'quicktags' => true
                ),
            )
        );

        /**
        * Contact
        */
        $cmb_contact = new_cmb2_box(
            array(
                'id'            => 'isystems_cases_contact_id',
                'title'         => __('Contact', 'isystemscombr'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-cases.blade.php'),
            )
        );

        //Featured Image
        $cmb_contact->add_field(
            array(
                'name'    => __('Featured Image', 'isystemscombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 1440x1000px | Aspect ratio: 36:25', 'isystemscombr'),
                'id'      => $prefix .'contact_img',
                'type'    => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' => __('Add Image', 'isystemscombr'), 
                ),
                'query_args' => array(
                    'type' => array(
                        'image/jpeg',
                        'image/png',
                        //'image/svg+xml',
                    ),
                ),
                'preview_size' => array(144, 100) 
            )
        ); 

        //Title
        $cmb_contact->add_field( 
            array(
                'name'       => __('Title', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'contact_title',
                'type'       => 'text',
            )
        );

        //Description
        $cmb_contact->add_field( 
            array(
                'name'       => __('Description', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'contact_desc',
                'type'       => 'wysiwyg',
                'options' => array(
                    'media_buttons' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 5),
                    'tabindex' => '',
                    'editor_css' => '',
                    'editor_class' => '',
                    'teeny' => true,
                    'tinymce' => true,
                    'quicktags' => true
                ),
            )
        );

        //Button Text
        $cmb_contact->add_field(
            array(
                'name'       => __('Button Text', 'isystemscombr'),
                'desc'       => __('Button call', 'isystemscombr'),
                'id'         => $prefix . 'contact_btn_text',
                'type'       => 'text',
            )
        );

        //Page Target
        $cmb_contact->add_field(
            array(
                'name'       => __('Page Target', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'contact_btn_page_id',
                'type'       => 'select',
                'options'    => Get_Post_array(array('post_type' => 'page'), true),
            )
        );
    }
);

/**
 * Blog / Home
 */
add_action(
    'cmb2_admin_init',
    function () {
        $prefix = '_isystems_blog_';

        /**
        * Header
        */
        $cmb_header = new_cmb2_box(
            array(
                'id'            => 'isystems_blog_header_id',
                'title'         => __('Header', 'isystemscombr'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on_cb' => 'Show_On_Home',
            )
        );

        //Title
        $cmb_header->add_field(
            array(
                'name'       => __('Title', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'header_title',
                'type'       => 'text',
            )
        );

        /**
        * Post Banner
        */
        $cmb_post_banner = new_cmb2_box(
            array(
                'id'            => 'isystems_blog_post_banner_id',
                'title'         => __('Post Banner', 'isystemscombr'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on_cb' => 'Show_On_Home',
            )
        );

        //Background Image
        $cmb_post_banner->add_field(
            array(
                'name'    => __('Background Image', 'isystemscombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 2880x1000px | Aspect ratio: 72:25', 'isystemscombr'),
                'id'      => $prefix .'post_banner_bkg_img',
                'type'    => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' => __('Add Image', 'isystemscombr'), 
                ),
                'query_args' => array(
                    'type' => array(
                        'image/jpeg',
                        'image/png',
                        //'image/svg+xml',
                    ),
                ),
                'preview_size' => array(288, 100) 
            ) 
        );

        //Title
        $cmb_post_banner->add_field(
            array(
                'name'       => __('Title', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'post_banner_title',
                'type'       => 'text',
            )
        );

        //Button Text
        $cmb_post_banner->add_field(
            array(
                'name'       => __('Button Text', 'isystemscombr'),
                'desc'       => __('Button call', 'isystemscombr'),
                'id'         => $prefix . 'post_banner_btn_text',
                'type'       => 'text',
            )
        );

        //Page Target
        $cmb_post_banner->add_field(
            array(
                'name'       => __('Page Target', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'post_banner_btn_page_id',
                'type'       => 'select',
                'options'    => Get_Post_array(array('post_type' => 'page'), true),
            )
        );
    }
);

/**
 * Blog / Single
 */
add_action(
    'cmb2_admin_init',
    function () {
        $prefix = '_isystems_post_';

        /**
        * Subtitle
        */
        $cmb_header = new_cmb2_box(
            array(
                'id'            => 'isystems_post_title_id',
                'title'         => __('Post Subtitle', 'isystemscombr'),
                'object_types'  => array('post'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
            )
        );

        //Title
        $cmb_header->add_field(
            array(
                'name'       => __('Subtitle', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'post_subtitle',
                'type'       => 'text',
            )
        );
    }
);

/**
 * Career
 */
add_action(
    'cmb2_admin_init',
    function () {
        $prefix = '_isystems_career_';

        /**
        * Header
        */
        $cmb_header = new_cmb2_box(
            array(
                'id'            => 'isystems_career_header_id',
                'title'         => __('Header', 'isystemscombr'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-carreira.blade.php'),
            )
        );

        //Featured Image
        $cmb_header->add_field(
            array(
                'name'    => __('Featured Image', 'isystemscombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 1440x1000px | Aspect ratio: 36:25', 'isystemscombr'),
                'id'      => $prefix .'header_img',
                'type'    => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' => __('Add Image', 'isystemscombr'), 
                ),
                'query_args' => array(
                    'type' => array(
                        'image/jpeg',
                        'image/png',
                        //'image/svg+xml',
                    ),
                ),
                'preview_size' => array(144, 100) 
            )
        ); 

        //Title
        $cmb_header->add_field(
            array(
                'name'       => __('Title', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'header_title',
                'type'       => 'text',
            )
        );

        //Description
        $cmb_header->add_field(
            array(
                'name'       => __('Description', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'header_desc',
                'type'       => 'wysiwyg',
                'options' => array(
                    'media_buttons' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 5),
                    'tabindex' => '',
                    'editor_css' => '',
                    'editor_class' => '',
                    'teeny' => true,
                    'tinymce' => true,
                    'quicktags' => true
                ),
            )
        );

        /**
        * Manifest
        */
        $cmb_manifest = new_cmb2_box(
            array(
                'id'            => 'isystems_career_manifest_id',
                'title'         => __('Manifest', 'isystemscombr'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-carreira.blade.php'),
            )
        );

        //Call
        $cmb_manifest->add_field(
            array(
                'name'       => __('Call (CTA)', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'manifest_cta',
                'type'       => 'wysiwyg',
                'options' => array(
                    'media_buttons' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 5),
                    'tabindex' => '',
                    'editor_css' => '',
                    'editor_class' => '',
                    'teeny' => true,
                    'tinymce' => true,
                    'quicktags' => true
                ),
            )
        );

        //Button Text
        $cmb_manifest->add_field(
            array(
                'name'       => __('Button Text', 'isystemscombr'),
                'desc'       => __('Button call', 'isystemscombr'),
                'id'         => $prefix . 'manifest_btn_text',
                'type'       => 'text',
            )
        );

        //Manifest Title
        $cmb_manifest->add_field(
            array(
                'name'       => __('Manifest Title', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'manifest_title',
                'type'       => 'text',
            )
        );

        //Manifest Subtitle
        $cmb_manifest->add_field(
            array(
                'name'       => __('Manifest Subtitle', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'manifest_subtitle',
                'type'       => 'text',
            )
        );

        //Manifest Content
        $cmb_manifest->add_field(
            array(
                'name'       => __('Manifest Content', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'manifest_content',
                'type'       => 'wysiwyg',
                'options' => array(
                    'media_buttons' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 5),
                    'tabindex' => '',
                    'editor_css' => '',
                    'editor_class' => '',
                    'teeny' => true,
                    'tinymce' => true,
                    'quicktags' => true
                ),
            )
        );

        /**
        * Our Founders
        */
        $cmb_ourfounders = new_cmb2_box(
            array(
                'id'            => 'isystems_career_ourfounders_id',
                'title'         => __('Our Founders', 'isystemscombr'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-carreira.blade.php'),
            )
        );

        //Title
        $cmb_ourfounders->add_field(
            array(
                'name'       => __('Title', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'ourfounders_title',
                'type'       => 'text',
            )
        );

        //Founders Group
        $ourfounders_id = $cmb_ourfounders->add_field(
            array(
                'id'          => $prefix . 'ourfounders_founder',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   =>__('Founder {#}', 'isystemscombr'),
                    'add_button'   =>__('Add Another Founder', 'isystemscombr'),
                    'remove_button' =>__('Remove Founder', 'isystemscombr'),
                    'sortable'      => true,
                ),
            )
        );

        //Founder Photo
        $cmb_ourfounders->add_group_field(
            $ourfounders_id,
            array(
                'name'        => __('Photo', 'isystemscombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 1600x1600px | Aspect ratio: 1:1', 'isystemscombr'),
                'id'          => 'photo',
                'type'        => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'isystemscombr'),
                ),
                'query_args' => array(
                    'type' => array(
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(240, 240)
            )
        );

        //Name
        $cmb_ourfounders->add_group_field(
            $ourfounders_id,
            array(
                'name'       => __('Name', 'isystemscombr'),
                'desc'       => '',
                'id'         => 'name',
                'type'       => 'text',
            )
        );

        //Role
        $cmb_ourfounders->add_group_field(
            $ourfounders_id,
            array(
                'name'       => __('Role', 'isystemscombr'),
                'desc'       => '',
                'id'         => 'role',
                'type'       => 'text',
            )
        );

        //Description
        $cmb_ourfounders->add_group_field(
            $ourfounders_id,
            array(
                'name'       => __('Description', 'isystemscombr'),
                'desc'       => '',
                'id'         => 'desc',
                'type'       => 'wysiwyg',
                'options' => array(
                    'media_buttons' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 5),
                    'tabindex' => '',
                    'editor_css' => '',
                    'editor_class' => '',
                    'teeny' => true,
                    'tinymce' => true,
                    'quicktags' => true
                ),
            )
        );

        /**
        * Mission
        */
        $cmb_mission = new_cmb2_box(
            array(
                'id'            => 'isystems_career_mission_id',
                'title'         => __('Mission', 'isystemscombr'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-carreira.blade.php'),
            )
        );

        //Featured Image
        $cmb_mission->add_field(
            array(
                'name'    => __('Featured Image', 'isystemscombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 1024x1138px | Aspect ratio: 9:10', 'isystemscombr'),
                'id'      => $prefix .'mission_img',
                'type'    => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' => __('Add Image', 'isystemscombr'), 
                ),
                'query_args' => array(
                    'type' => array(
                        'image/jpeg',
                        'image/png',
                        //'image/svg+xml',
                    ),
                ),
                'preview_size' => array(102, 113) 
            )
        ); 

        //Title
        $cmb_mission->add_field(
            array(
                'name'       => __('Title', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'mission_title',
                'type'       => 'text',
            )
        );

        //Description
        $cmb_mission->add_field(
            array(
                'name'       => __('Description', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'mission_desc',
                'type'       => 'wysiwyg',
                'options' => array(
                    'media_buttons' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 5),
                    'tabindex' => '',
                    'editor_css' => '',
                    'editor_class' => '',
                    'teeny' => true,
                    'tinymce' => true,
                    'quicktags' => true
                ),
            )
        );

        /**
        * Vision
        */
        $cmb_vision = new_cmb2_box(
            array(
                'id'            => 'isystems_career_vision_id',
                'title'         => __('Vision', 'isystemscombr'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-carreira.blade.php'),
            )
        );

        //Featured Image
        $cmb_vision->add_field(
            array(
                'name'    => __('Featured Image', 'isystemscombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 1024x896px | Aspect ratio: 8:7', 'isystemscombr'),
                'id'      => $prefix .'vision_img',
                'type'    => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' => __('Add Image', 'isystemscombr'), 
                ),
                'query_args' => array(
                    'type' => array(
                        'image/jpeg',
                        'image/png',
                        //'image/svg+xml',
                    ),
                ),
                'preview_size' => array(102, 89) 
            )
        ); 

        //Title
        $cmb_vision->add_field(
            array(
                'name'       => __('Title', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'vision_title',
                'type'       => 'text',
            )
        );

        //Description
        $cmb_vision->add_field(
            array(
                'name'       => __('Description', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'vision_desc',
                'type'       => 'wysiwyg',
                'options' => array(
                    'media_buttons' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 5),
                    'tabindex' => '',
                    'editor_css' => '',
                    'editor_class' => '',
                    'teeny' => true,
                    'tinymce' => true,
                    'quicktags' => true
                ),
            )
        );

        /**
        * Values
        */
        $cmb_values = new_cmb2_box(
            array(
                'id'            => 'isystems_career_values_id',
                'title'         => __('Values', 'isystemscombr'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-carreira.blade.php'),
            )
        );

        //Featured Image
        $cmb_values->add_field(
            array(
                'name'    => __('Featured Image', 'isystemscombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 1024x812px | Aspect ratio: 24:19', 'isystemscombr'),
                'id'      => $prefix .'values_img',
                'type'    => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' => __('Add Image', 'isystemscombr'), 
                ),
                'query_args' => array(
                    
                    'type' => array(
                        'image/jpeg',
                        'image/png',
                        //'image/svg+xml',
                    ),
                ),
                'preview_size' => array(102, 81) 
            )
        ); 

        //Title
        $cmb_values->add_field(
            array(
                'name'       => __('Title', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'values_title',
                'type'       => 'text',
            )
        );

        //Description
        $cmb_values->add_field(
            array(
                'name'       => __('Description', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'values_desc',
                'type'       => 'wysiwyg',
                'options' => array(
                    'media_buttons' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 5),
                    'tabindex' => '',
                    'editor_css' => '',
                    'editor_class' => '',
                    'teeny' => true,
                    'tinymce' => true,
                    'quicktags' => true
                ),
            )
        );

        /**
        * Values Infographic
        */
        $cmb_valuesinfo = new_cmb2_box(
            array(
                'id'            => 'isystems_career_valuesinfo_id',
                'title'         => __('Values Infographic', 'isystemscombr'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-carreira.blade.php'),
            )
        );

        //Title Icon
        $cmb_valuesinfo->add_field(
            array (
                'name'    => __('Title Icon', 'isystemscombr'),
                'desc'    => __('Format: SVG (recommended), PNG | Recommended size: 320x320px | Aspect ratio: 1:1', 'isystemscombr'),
                'id'      => $prefix . 'valuesinfo_icon',
                'type'    => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' => __('Add Image', 'isystemscombr'), 
                ),
                'query_args' => array(
                    'type' => array(
                        //'image/jpeg',
                        'image/png',
                        'image/svg+xml',
                    ),
                ),
                'preview_size' => array(64, 64) 
            ) 
        );

        //Title
        $cmb_valuesinfo->add_field(
            array (
                'name'       => __('Title', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'valuesinfo_title',
                'type'       => 'text',
            )
        );

        //Values Group
        $values_id = $cmb_valuesinfo->add_field(
            array(
                'id'          => $prefix . 'valuesinfo_values',
                'type'        => 'group',
                'description' => __('Values', 'isystemscombr'),
                'options'     => array(
                    'group_title'   => __('Value {#}', 'isystemscombr'),
                    'add_button'    => __('Add Another Value', 'isystemscombr'),
                    'remove_button' => __('Remove Value', 'isystemscombr'),
                    'sortable'      => true,
                ),
            )
        );

        //Icon
        $cmb_valuesinfo->add_group_field(
            $values_id,
            array (
                'name'    => __('Icon', 'isystemscombr'),
                'desc'    => __('Format: SVG (recommended), PNG | Recommended size: 320x320px | Aspect ratio: 1:1', 'isystemscombr'),
                'id'      => 'icon',
                'type'    => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' => __('Add Image', 'isystemscombr'), 
                ),
                'query_args' => array(
                    'type' => array(
                        //'image/jpeg',
                        'image/png',
                        'image/svg+xml',
                    ),
                ),
                'preview_size' => array(64, 64) 
            ) 
        );

        //Title
        $cmb_valuesinfo->add_group_field(
            $values_id,
            array (
                'name'       => __('Title', 'isystemscombr'),
                'desc'       => '',
                'id'         => 'title',
                'type'       => 'text',
            )
        );
        
        //Description
        $cmb_valuesinfo->add_group_field(
            $values_id,
            array (
                'name'       => __('Description', 'isystemscombr'),
                'desc'       => '',
                'id'         => 'desc',
                'type'       => 'text',
            )
        );

        /**
        * Pillars
        */
        $cmb_pillars = new_cmb2_box(
            array(
                'id'            => 'isystems_career_pillars_id',
                'title'         => __('Pillars', 'isystemscombr'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-carreira.blade.php'),
            )
        );

        //Title
        $cmb_pillars->add_field( 
            array(
                'name'       => __('Title', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'pillars_title',
                'type'       => 'text',
            )
        );

        //Pillars Group
        $pillars_id = $cmb_pillars->add_field(
            array(
                'id'          => $prefix . 'pillars_pillars',
                'type'        => 'group',
                'description' => __('Values', 'isystemscombr'),
                'options'     => array(
                    'group_title'   => __('Pillar {#}', 'isystemscombr'),
                    'add_button'    => __('Add Another Pillar', 'isystemscombr'),
                    'remove_button' => __('Remove Pillar', 'isystemscombr'),
                    'sortable'      => true,
                ),
            )
        );

        //Featured Image
        $cmb_pillars->add_group_field(
            $pillars_id,
            array (
                'name'    => __('Featured Image', 'isystemscombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 1600x1600px | Aspect ratio: 1:1', 'isystemscombr'),
                'id'      => 'img',
                'type'    => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' => __('Add Image', 'isystemscombr'), 
                ),
                'query_args' => array(
                    'type' => array(
                        'image/jpeg',
                        'image/png',
                        //'image/svg+xml',
                    ),
                ),
                'preview_size' => array(144, 144) 
            ) 
        );

        //Title
        $cmb_pillars->add_group_field(
            $pillars_id,
            array (
                'name'       => __('Title', 'isystemscombr'),
                'desc'       => '',
                'id'         => 'title',
                'type'       => 'text',
            )
        );
        
        //Description
        $cmb_pillars->add_group_field(
            $pillars_id,
            array (
                'name'       => __('Description', 'isystemscombr'),
                'desc'       => '',
                'id'         => 'desc',
                'type'       => 'textarea_small',
            )
        );

        /**
        * Testimonials
        */
        $cmb_testimonials = new_cmb2_box(
            array(
                'id'            => 'isystems_career_testimonials_id',
                'title'         => __('Testimonials', 'isystemscombr'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-carreira.blade.php'),
            )
        );

        //Title
        $cmb_testimonials->add_field( 
            array(
                'name'       => __('Title', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'testimonials_title',
                'type'       => 'wysiwyg',
                'options' => array(
                    'media_buttons' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 2),
                    'tabindex' => '',
                    'editor_css' => '',
                    'editor_class' => '',
                    'teeny' => true,
                    'tinymce' => true,
                    'quicktags' => true
                ),
            )
        );
        
        //Testimonials Group
        $testimonial_id = $cmb_testimonials->add_field(
            array(
                'id'          => $prefix . 'testimonials_testimonial',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   => __('Testimonial {#}', 'isystemscombr'),
                    'add_button'    => __('Add Another Testimonial', 'isystemscombr'),
                    'remove_button' => __('Remove Testimonial', 'isystemscombr'),
                    'sortable'      => true,
                ),
            )
        );

        // Testimonial Client
        $cmb_testimonials->add_group_field(
            $testimonial_id,
            array (
                'name'       => __('Name', 'isystemscombr'),
                'desc'       => '',
                'id'         => 'name',
                'type'       => 'text',
            )
        );

        // Testimonial Role
        $cmb_testimonials->add_group_field(
            $testimonial_id,
            array (
                'name'       => __('Role', 'isystemscombr'),
                'desc'       => '',
                'id'         => 'role',
                'type'       => 'text',
            )
        );

        //Testimonial Type
        $cmb_testimonials->add_group_field(
            $testimonial_id,
            array(
                'name'       => __('Testimonial Type', 'isystemscombr'),
                'desc'       => __('Select testimonial type', 'isystemscombr'),
                'id'         => 'type',
                'type'             => 'radio',
                'show_option_none' => false,
                'options'          => array(
                    'text'   => __('Text', 'isystemscombr'),
                    'video_url'  => __('Video Modal', 'isystemscombr'),
                ),
                'default' => 'text',
            )
        );

        // Testimonial Description
        $cmb_testimonials->add_group_field(
            $testimonial_id,
            array (
                'name'       => __('Testimonial', 'isystemscombr'),
                'desc'       => '',
                'id'         => 'content',
                'type'       => 'wysiwyg',
                'options' => array(
                    'media_buttons' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 5),
                    'tabindex' => '',
                    'editor_css' => '',
                    'editor_class' => '',
                    'teeny' => true,
                    'tinymce' => true,
                    'quicktags' => true
                ),
            )
        );

        //Button Text
        $cmb_testimonials->add_group_field(
            $testimonial_id,
            array(
                'name'       => __('Button Text', 'isystemscombr'),
                'desc'       => __('Button call', 'isystemscombr'),
                'id'         => 'btn_text',
                'type'       => 'text',
            )
        );

        //Button Link
        $cmb_testimonials->add_group_field(
            $testimonial_id,
            array(
                'name'       => __('Testimonial Video (URL)', 'isystemscombr'),
                'desc'       => '',
                'id'         => 'btn_video_url',
                'type'       => 'text',
            )
        );
        
        /**
        * Jobs
        */
        $cmb_jobs = new_cmb2_box(
            array(
                'id'            => 'isystems_career_jobs_id',
                'title'         => __('Jobs', 'isystemscombr'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-carreira.blade.php'),
            )
        );

        //Title
        $cmb_jobs->add_field( 
            array(
                'name'       => __('Title', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'jobs_title',
                'type'       => 'text',
            )
        );

        //API
        $cmb_jobs->add_field( 
            array(
                'name'       => __('API Access Token', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'jobs_api_token',
                'type'       => 'text',
            )
        );

        //Button Text
        $cmb_jobs->add_field(
            array(
                'name'       => __('Button Text', 'isystemscombr'),
                'desc'       => __('Button call', 'isystemscombr'),
                'id'         => $prefix . 'jobs_btn_text',
                'type'       => 'text',
            )
        );

        //Button URL
        $cmb_jobs->add_field(
            array(
                'name'       => __('Button Link (URL)', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'jobs_btn_url',
                'type'       => 'text',
            )
        );
    }
);

/**
 * Contact
 */
add_action(
    'cmb2_admin_init',
    function () {
        $prefix = '_isystems_contact_';

        /**
        * Header
        */
        $cmb_header = new_cmb2_box(
            array(
                'id'            => 'isystems_contact_header_id',
                'title'         => __('Header', 'isystemscombr'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-contato.blade.php'),
            )
        );

        //Background Image
        $cmb_header->add_field(
            array(
                'name'    => __('Background Image', 'isystemscombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 2880x1000px | Aspect ratio: 72:25', 'isystemscombr'),
                'id'      => $prefix .'header_img',
                'type'    => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' => __('Add Image', 'isystemscombr'), 
                ),
                'query_args' => array(
                    'type' => array(
                        'image/jpeg',
                        'image/png',
                        //'image/svg+xml',
                    ),
                ),
                'preview_size' => array(288, 100) 
            )
        ); 

        //Title
        $cmb_header->add_field(
            array(
                'name'       => __('Title', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'header_title',
                'type'       => 'text',
            )
        );
    
        //Description
        $cmb_header->add_field(
            array(
                'name'       => __('Description', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'header_desc',
                'type'       => 'wysiwyg',
                'options' => array(
                    'media_buttons' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 5),
                    'tabindex' => '',
                    'editor_css' => '',
                    'editor_class' => '',
                    'teeny' => true,
                    'tinymce' => true,
                    'quicktags' => true
                ),
            )
        );

        /**
        * Form
        */
        $cmb_form = new_cmb2_box(
            array(
                'id'            => 'isystems_contact_form_id',
                'title'         => __('Form', 'isystemscombr'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-contato.blade.php'),
            )
        );

        //Title
        $cmb_form->add_field(
            array(
                'name'       => __('Title', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'form_title',
                'type'       => 'text',
            )
        );
    

        //Shortcode
        $cmb_form->add_field( 
            array(
                'name'       => __('Form Shortcode', 'isystemscombr'),
                'desc'       => 'Contact Form 7 Shortcode',
                'id'         => $prefix . 'form_shortcode',
                'type'       => 'text',
            )
        );

        /**
        * Address
        */
        $cmb_address = new_cmb2_box(
            array(
                'id'            => 'isystems_contact_address_id',
                'title'         => __('Address', 'isystemscombr'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-contato.blade.php'),
            )
        );

        //Map (iframe)
        $cmb_address->add_field(
            array(
                'name'       => __('Map (iframe)', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'map_iframe',
                'type'       => 'textarea_code',
            )
        );

        //Map Footer Link (URL)
        $cmb_address->add_field(
            array(
                'name'       => __('Footer Map Link (URL)', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'map_url',
                'type'       => 'text_url',
            )
        );

        //Address
        $cmb_address->add_field(
            array(
                'name'       => __('Address', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'address',
                'type'       => 'text',
            )
        );

        //Footer Address
        $cmb_address->add_field(
            array(
                'name'       => __('Footer Address', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'footer_address',
                'type'       => 'text',
            )
        );

        //Phone
        $cmb_address->add_field(
            array(
                'name'       => __('Phone', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'phone',
                'type'       => 'text_medium',
            )
        );

        //Email
        $cmb_address->add_field(
            array(
                'name'       => __('Email', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'email',
                'type'       => 'text_email',
            )
        );

        /**
        * Social Networks
        */
        $cmb_social = new_cmb2_box(
            array(
                'id'            => 'isystems_contact_social_id',
                'title'         => __('Social Networks', 'isystemscombr'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-contato.blade.php'),
            )
        );

        //Facebook (URL)
        $cmb_social->add_field(
            array(
                'name'       => __('Facebook (URL)', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'facebook_url',
                'type'       => 'text_url',
            )
        );

        //LinkedIn (URL)
        $cmb_social->add_field(
            array(
                'name'       => __('LinkedIn (URL)', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'linkedin_url',
                'type'       => 'text_url',
            )
        );

        //Youtube (URL)
        $cmb_social->add_field(
            array(
                'name'       => __('Youtube (URL)', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'youtube_url',
                'type'       => 'text_url',
            )
        );
    }
);

/**
 * Privacy Policy
 */
add_action(
    'cmb2_admin_init',
    function () {
        $prefix = '_isystems_privacy_';

        /**
        * Main
        */
        $cmb_main = new_cmb2_box(
            array(
                'id'            => 'isystems_privacy_main_id',
                'title'         => __('Main', 'isystemscombr'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on_cb' => 'Show_On_Privacy',
            )
        );

        //Title
        $cmb_main->add_field(
            array(
                'name'       => __('Title', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'main_title',
                'type'       => 'text',
            )
        );

        //Description
        $cmb_main->add_field(
            array(
                'name'       => __('Description', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'main_desc',
                'type'       => 'wysiwyg',
                'options' => array(
                    'media_buttons' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 5),
                    'tabindex' => '',
                    'editor_css' => '',
                    'editor_class' => '',
                    'teeny' => true,
                    'tinymce' => true,
                    'quicktags' => true
                ),
            )
        );

        //Topics Group
        $topics_id = $cmb_main->add_field(
            array(
                'id'          => $prefix . 'main_topics',
                'type'        => 'group',
                'description' => __('Topics', 'isystemscombr'),
                'options'     => array(
                    'group_title'   => __('Topic {#}', 'isystemscombr'),
                    'add_button'    => __('Add Another Topic', 'isystemscombr'),
                    'remove_button' => __('Remove Topic', 'isystemscombr'),
                    'sortable'      => true,
                ),
            )
        );

        // Topic Title
        $cmb_main->add_group_field(
            $topics_id,
            array (
                'name'       => __('Title', 'isystemscombr'),
                'desc'       => '',
                'id'         => 'title',
                'type'       => 'text',
            )
        );
        
        // Topic Description
        $cmb_main->add_group_field(
            $topics_id,
            array (
                'name'       => __('Description', 'isystemscombr'),
                'desc'       => '',
                'id'         => 'desc',
                'type'       => 'wysiwyg',
                'options' => array(
                    'media_buttons' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 5),
                    'tabindex' => '',
                    'editor_css' => '',
                    'editor_class' => '',
                    'teeny' => true,
                    'tinymce' => true,
                    'quicktags' => true
                ),
            )
        );

        /**
        * Cookie Policy I.Systems
        */
        $cmb_cookie_policy = new_cmb2_box(
            array(
                'id'            => 'isystems_privacy_cookie_policy_id',
                'title'         => __('Cookie Policy I.Systems', 'isystemscombr'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on_cb' => 'Show_On_Privacy',
            )
        );

        //Title
        $cmb_cookie_policy->add_field(
            array(
                'name'       => __('Title', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'cookie_policy_title',
                'type'       => 'text',
            )
        );

        //Description
        $cmb_cookie_policy->add_field(
            array(
                'name'       => __('Description', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'cookie_policy_desc',
                'type'       => 'wysiwyg',
                'options' => array(
                    'media_buttons' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 5),
                    'tabindex' => '',
                    'editor_css' => '',
                    'editor_class' => '',
                    'teeny' => true,
                    'tinymce' => true,
                    'quicktags' => true
                ),
            )
        );

        //Topics Group
        $topics_id = $cmb_cookie_policy->add_field(
            array(
                'id'          => $prefix . 'cookie_policy_topics',
                'type'        => 'group',
                'description' => __('Topics', 'isystemscombr'),
                'options'     => array(
                    'group_title'   => __('Topic {#}', 'isystemscombr'),
                    'add_button'    => __('Add Another Topic', 'isystemscombr'),
                    'remove_button' => __('Remove Topic', 'isystemscombr'),
                    'sortable'      => true,
                ),
            )
        );

        // Topic Title
        $cmb_cookie_policy->add_group_field(
            $topics_id,
            array (
                'name'       => __('Title', 'isystemscombr'),
                'desc'       => '',
                'id'         => 'title',
                'type'       => 'text',
            )
        );
        
        // Topic Description
        $cmb_cookie_policy->add_group_field(
            $topics_id,
            array (
                'name'       => __('Description', 'isystemscombr'),
                'desc'       => '',
                'id'         => 'desc',
                'type'       => 'wysiwyg',
                'options' => array(
                    'media_buttons' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 5),
                    'tabindex' => '',
                    'editor_css' => '',
                    'editor_class' => '',
                    'teeny' => true,
                    'tinymce' => true,
                    'quicktags' => true
                ),
            )
        );

        /**
        * Cookies we set
        */
        $cmb_cookies_we_set = new_cmb2_box(
            array(
                'id'            => 'isystems_privacy_cookies_we_set_id',
                'title'         => __('Cookies we set', 'isystemscombr'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on_cb' => 'Show_On_Privacy',
            )
        );

        //Title
        $cmb_cookies_we_set->add_field(
            array(
                'name'       => __('Title', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'cookies_we_set_title',
                'type'       => 'text',
            )
        );

        //Topics Group
        $topics_id = $cmb_cookies_we_set->add_field(
            array(
                'id'          => $prefix . 'cookies_we_set_topics',
                'type'        => 'group',
                'description' => __('Topics', 'isystemscombr'),
                'options'     => array(
                    'group_title'   => __('Topic {#}', 'isystemscombr'),
                    'add_button'    => __('Add Another Topic', 'isystemscombr'),
                    'remove_button' => __('Remove Topic', 'isystemscombr'),
                    'sortable'      => true,
                ),
            )
        );

        // Topic Title
        $cmb_cookies_we_set->add_group_field(
            $topics_id,
            array (
                'name'       => __('Title', 'isystemscombr'),
                'desc'       => '',
                'id'         => 'title',
                'type'       => 'text',
            )
        );
        
        // Topic Description
        $cmb_cookies_we_set->add_group_field(
            $topics_id,
            array (
                'name'       => __('Description', 'isystemscombr'),
                'desc'       => '',
                'id'         => 'desc',
                'type'       => 'wysiwyg',
                'options' => array(
                    'media_buttons' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 5),
                    'tabindex' => '',
                    'editor_css' => '',
                    'editor_class' => '',
                    'teeny' => true,
                    'tinymce' => true,
                    'quicktags' => true
                ),
            )
        );

        /**
        * Google Analytics
        */
        $cmb_analytics = new_cmb2_box(
            array(
                'id'            => 'isystems_privacy_analytics_id',
                'title'         => __('Google Analytics', 'isystemscombr'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on_cb' => 'Show_On_Privacy',
            )
        );

        //Title
        $cmb_analytics->add_field(
            array(
                'name'       => __('Title', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'analytics_title',
                'type'       => 'text',
            )
        );

        //Description
        $cmb_analytics->add_field(
            array(
                'name'       => __('Description', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'analytics_desc',
                'type'       => 'wysiwyg',
                'options' => array(
                    'media_buttons' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 5),
                    'tabindex' => '',
                    'editor_css' => '',
                    'editor_class' => '',
                    'teeny' => true,
                    'tinymce' => true,
                    'quicktags' => true
                ),
            )
        );

        /**
        * User Commitment
        */
        $cmb_user = new_cmb2_box(
            array(
                'id'            => 'isystems_privacy_user_id',
                'title'         => __('User Commitment', 'isystemscombr'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on_cb' => 'Show_On_Privacy',
            )
        );

        //Title
        $cmb_user->add_field(
            array(
                'name'       => __('Title', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'user_title',
                'type'       => 'text',
            )
        );

        //Description
        $cmb_user->add_field(
            array(
                'name'       => __('Description', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'user_desc',
                'type'       => 'wysiwyg',
                'options' => array(
                    'media_buttons' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 5),
                    'tabindex' => '',
                    'editor_css' => '',
                    'editor_class' => '',
                    'teeny' => true,
                    'tinymce' => true,
                    'quicktags' => true
                ),
            )
        );

        /**
        * More information
        */
        $cmb_more_info = new_cmb2_box(
            array(
                'id'            => 'isystems_privacy_more_info_id',
                'title'         => __('More information', 'isystemscombr'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on_cb' => 'Show_On_Privacy',
            )
        );

        //Title
        $cmb_more_info->add_field(
            array(
                'name'       => __('Title', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'more_info_title',
                'type'       => 'text',
            )
        );

        //Description
        $cmb_more_info->add_field(
            array(
                'name'       => __('Description', 'isystemscombr'),
                'desc'       => '',
                'id'         => $prefix . 'more_info_desc',
                'type'       => 'wysiwyg',
                'options' => array(
                    'media_buttons' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 5),
                    'tabindex' => '',
                    'editor_css' => '',
                    'editor_class' => '',
                    'teeny' => true,
                    'tinymce' => true,
                    'quicktags' => true
                ),
            )
        );
    }
);
